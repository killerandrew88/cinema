package Table;

import java.awt.Dimension;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import DataStructure.Director;
import DataStructure.Movie;
import DataStructure.Proj_Reserv_Movie;

public class ReservationTableUserIdModel  extends AbstractTableModel implements TableModelListener {
	
	
		private TableListener tableListener = null;
		private ArrayList ReservationList= new ArrayList();
		private String[] columnNames = {"TITLE","DAY","START TIME","DURATION","ROOM",};
		private Proj_Reserv_Movie res; 

		
		public ReservationTableUserIdModel(ArrayList ReservationList){
			this.ReservationList=ReservationList;
		}
		
		/**
		 * update the new ReservationList if it has been changed
		 * @param ReservationList
		 */
		public void updateReservation(ArrayList ReservationList){
			this.ReservationList=ReservationList;
			this.notifyChanges();
		}
		
		public void removeRow(int row) {
		    ReservationList.remove(row);
		    this.notifyChanges();
		}
		
		@Override		    
		public int getColumnCount() {
			return columnNames.length;
	    }

		@Override
		public int getRowCount() {
		      return ReservationList.size();
		}

		public Dimension getPreferredScrollableViewportSize() {
	        return new Dimension(320, 240);
	    }
		
		@Override
	    public String getColumnName(int col) {
	        return columnNames[col];
	    }


		
		public boolean isCellEditable(int row, int col) {
			return false;
		}	

		@Override
		public void tableChanged(TableModelEvent e) {
			int row = e.getFirstRow();
			int col = e.getColumn();
			TableModel model = (TableModel) e.getSource();
			Object data = model.getValueAt(row,col);
			this.setValueAt(data,row,col);
		}

		
//		questo metodo � da mantenere?
		
//		/**    
//		 * method that removes a row from the table and also the film from the arrayList
//		 * @param row
//		 */
//		public void removeRow(int row) {
//			ReservationList.remove(row);
//			fireTableRowsDeleted(row, row);
//			notifyChanges();
//		}
		
		public void notifyChanges(){
			this.fireTableDataChanged();
		}
		

		public void setTabelListener(TableListener tableListener) {
			this.tableListener = tableListener;
		}

		
		@Override
		public Object getValueAt(int row, int col) {
			res=(Proj_Reserv_Movie) ReservationList.get(row);
			
			switch(col) {
			
			
			case 0: return res.getTitle();
			
			case 1: return res.getProj_day();
			
			case 2: return res.getProj_startTime();
			
			case 3: return res.getDuration() + " min.";
			
			case 4: return res.getRoomId();
				
			}
			
			return null;
		}
		
		
	}


