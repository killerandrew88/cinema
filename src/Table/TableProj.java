package Table;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TableProj extends JTable{
private TableListener tableListener;
	
	public TableProj(TableModel tm){
		super(tm);
		this.setColumnSelectionAllowed(false);
	}
	
	
	public void setTableListener(TableListener listener){
		this.tableListener = listener;
		ReservationTableByProjModel model = (ReservationTableByProjModel) this.getModel();
		model.setTabelListener(this.tableListener);
	}

}


