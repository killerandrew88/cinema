package Table;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TableEmail extends JTable{
private TableListener tableListener;
	
	public TableEmail(TableModel tm){
		super(tm);
		this.setColumnSelectionAllowed(false);
	}
	
	
	public void setTableListener(TableListener listener){
		this.tableListener = listener;
		ReservationTableEmailModel model = (ReservationTableEmailModel) this.getModel();
		model.setTabelListener(this.tableListener);
	}

}


