package Table;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TableTitle extends JTable{

private TableListener tableListener;
	
	public TableTitle(TableModel tm){
		super(tm);
		this.setColumnSelectionAllowed(false);
	}
	
	
	public void setTableListener(TableListener listener){
		this.tableListener = listener;
		MovieTableTitleModel model = (MovieTableTitleModel) this.getModel();
		model.setTabelListener(this.tableListener);
	}
}
