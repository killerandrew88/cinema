package Table;

import javax.swing.JTable;
import javax.swing.table.TableModel;

public class TableUserId extends JTable{
private TableListener tableListener;
	
	public TableUserId(TableModel tm){
		super(tm);
		this.setColumnSelectionAllowed(false);
	}
	
	
	public void setTableListener(TableListener listener){
		this.tableListener = listener;
		ReservationTableUserIdModel model = (ReservationTableUserIdModel) this.getModel();
		model.setTabelListener(this.tableListener);
	}

}
