package Table.projectionTables;

import javax.swing.table.AbstractTableModel;

import DataStructure.Projection;
import DataStructure.ProjectionCollection;

public class ProjectionTableModel extends AbstractTableModel {
	
	private String[] columnNames = {"Date.","Proj. Time","Movie","3D","Duration","Price","Room Number","Proj. ID","Movie ID"};
	private ProjectionCollection data;
	
	public ProjectionTableModel(ProjectionCollection data) {
		this.data = data;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return this.data.getCollection().size();
	}
	
	public void removeRow(int row) {
	    data.getCollection().remove(row);
	    this.notifyChanges();
	}

	@Override
	public Object getValueAt(int row, int col) {
		Projection p = data.getCollection().get(row);
        switch (col) {
        case 0:return (Object) p.getDay();
        case 1:return (Object) p.getStartTime();
        case 2:return (Object) p.getMovie().getTitle();
        case 3: if (p.getThreeD_Option() == true)
        			return (Object) "Yes";
        			else return (Object) "No";
        case 4:return (Object) (p.getMovie().getDuration());
        case 5:return (Object) (p.getPrice());
        case 6:return (Object) (p.getRoom().getRoomNum());
        case 7:return (Object) (p.getProjID());
        case 8:return (Object) (p.getMovie().getId());
        }
        return null;
	}
	
	@Override
	public String getColumnName(int column) {
	    	  return columnNames[column];
	}
	 
	public void notifyChanges(){
		this.fireTableDataChanged();
	}

}
