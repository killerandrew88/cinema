package Table.projectionTables;

import java.awt.Component;
import java.awt.Font;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

public class IntelligentTable extends JTable {
	
	public IntelligentTable(TableModel dm) {
		super(dm);
		this.setColumnSelectionAllowed(false);
		//set selection to single cell
		this.setCellSelectionEnabled(false);
		this.setRowSelectionAllowed(true);
		
		
		this.setRowHeight(25);
		this.setFont(new Font("Calibri", Font.PLAIN, 20));
		
	    ListSelectionModel cellSelectionModel = this.getSelectionModel();
	    cellSelectionModel.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	
	    
//	    this.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		 
       
//	    this.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	 
	    
	    
	    
	    
	}
	
	
	
	public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component component = super.prepareRenderer(renderer, row, column);
        int rendererWidth = component.getPreferredSize().width;
        TableColumn tableColumn = getColumnModel().getColumn(column);
        tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
        return component;
     }
	    
}

