package Table;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import DataStructure.Proj_Reserv_Movie;

public class ReservationTableByProjModel extends AbstractTableModel implements TableModelListener {
		
		
		private TableListener tableListener = null;
		private ArrayList ReservationList= new ArrayList();
		private String[] columnNames = {"PROJECTION ID","TITLE","EMAIL","USER_ID","DAY","START TIME","ROOM",};
		private Proj_Reserv_Movie res; 

		
		public ReservationTableByProjModel(ArrayList ReservationList){
			this.ReservationList=ReservationList;
			this.notifyChanges();
		}
		
		/**
		 * update the new ReservationList if it has been changed
		 * @param ReservationList
		 */
		public void updateReservation(ArrayList ReservationList){
			this.ReservationList=ReservationList;
			this.notifyChanges();
		}
		
		@Override		    
		public int getColumnCount() {
			return columnNames.length;
	    }

		public void removeRow(int row) {
		    ReservationList.remove(row);
		    this.notifyChanges();
		}
		
		@Override
		public int getRowCount() {
		      return ReservationList.size();
		}

		public Dimension getPreferredScrollableViewportSize() {
	        return new Dimension(320, 240);
	    }
		
		@Override
	    public String getColumnName(int col) {
	        return columnNames[col];
	    }


		
		public boolean isCellEditable(int row, int col) {
			return false;
		}	

		@Override
		public void tableChanged(TableModelEvent e) {
			int row = e.getFirstRow();
			int col = e.getColumn();
			TableModel model = (TableModel) e.getSource();
			Object data = model.getValueAt(row,col);
			this.setValueAt(data,row,col);
		}

		
		
		public void notifyChanges(){
			this.fireTableDataChanged();
		}
		

		public void setTabelListener(TableListener tableListener) {
			this.tableListener = tableListener;
		}

		
		@Override
		public Object getValueAt(int row, int col) {
			res=(Proj_Reserv_Movie) ReservationList.get(row);
			
			switch(col) {
			
			case 0: return res.getProj_id();
			
			case 1: return res.getTitle();
			
			case 2: return res.getEmail();
			
			case 3: return res.getCust_taxno();
			
			case 4: return res.getProj_day();
			
			case 5: return res.getProj_startTime();
			
			case 6: return res.getRoomId();
				
			}
			
			return null;
		}
		
		
	}





