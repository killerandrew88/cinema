package Table;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import DataStructure.Movie;
import DataStructure.Proj_Reserv_Movie;

public class MovieTableTitleModel extends AbstractTableModel implements TableModelListener{
	
	private TableListener tableListener = null;
	private ArrayList MovieList= new ArrayList();
	private Movie movie;
	private String[] columnNames = {"MOVIE ID","TITLE","DATE OF PRODUCTION","DURATION","COUNTRY","GENRE","DIRECTOR",};

	
	public MovieTableTitleModel(ArrayList MovieList){
		this.MovieList=MovieList;
		this.notifyChanges();
	}
	
	/**
	 * update the new ReservationList if it has been changed
	 * @param ReservationList
	 */
	public void updateMovie(ArrayList movieList){
		this.MovieList=movieList;
		this.notifyChanges();
	}
	
	public void removeRow(int row) {
	    MovieList.remove(row);
	    this.notifyChanges();
	}
	
	@Override		    
	public int getColumnCount() {
		return columnNames.length;
    }

	@Override
	public int getRowCount() {
	      return MovieList.size();
	}

	public Dimension getPreferredScrollableViewportSize() {
        return new Dimension(320, 240);
    }
	
	@Override
    public String getColumnName(int col) {
        return columnNames[col];
    }


	
	public boolean isCellEditable(int row, int col) {
		return false;
	}	

	@Override
	public void tableChanged(TableModelEvent e) {
		int row = e.getFirstRow();
		int col = e.getColumn();
		TableModel model = (TableModel) e.getSource();
		Object data = model.getValueAt(row,col);
		this.setValueAt(data,row,col);
	}

	
	
	public void notifyChanges(){
		this.fireTableDataChanged();
	}
	

	public void setTabelListener(TableListener tableListener) {
		this.tableListener = tableListener;
	}

	
	@Override
	public Object getValueAt(int row, int col) {
		String director,genre;
		movie=(Movie) MovieList.get(row);
		
		switch(col) {
		
		case 0: return movie.getId();
		
		case 1: return movie.getTitle();
		
		case 2: return movie.getDateOfProduction();
		
		case 3: return movie.getDuration();
		
		case 4: return movie.getCountry();
		
		case 5: genre= movie.getGenre().toString().substring(1, movie.getGenre().toString().length()-1);
			return genre;
		
		case 6: director= movie.getDirector().toString().substring(1, movie.getDirector().toString().length()-1);
			return director;
		}
		
		return null;
	}
	
	
}

