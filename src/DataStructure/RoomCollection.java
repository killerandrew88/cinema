package DataStructure;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import Utilities.DBManager;

public class RoomCollection {
	private ArrayList<Room> roomList;
	DBManager dbm;
	
	public RoomCollection() {
		dbm = new DBManager();
		roomList = new ArrayList<Room>();
	}
	
	
	public void loadList(ResultSet rs) {
		if (rs!=null){
			try {
				while (rs.next()) {
					Room room = new Room(rs.getInt(1),rs.getInt(2),rs.getInt(3));
					this.roomList.add(room);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	public ArrayList<Room> getRoomList() {
		return roomList;
	}


	public void setRoomList(ArrayList<Room> roomList) {
		this.roomList = roomList;
	}


	public DBManager getDbm() {
		return dbm;
	}


	public void setDbm(DBManager dbm) {
		this.dbm = dbm;
	}

}
