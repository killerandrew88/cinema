package DataStructure;

import java.time.LocalDate;

public class Customer extends CUser{
	
	private String tax_no;
	private LocalDate reg_date;
	private int id_mem;
	private Membership membership;
	
	
	public Customer(){
		super();
	}
	
	/**
	 * Constructor of Customer,subclass of CUser. First read data of CUser from Database,
	 * then if type = "u" then create a user with that data
	 * and read also the table Customer with corresponding id, and read its registration date and membership id(if present).
	 * @param tax_no
	 * @param name
	 * @param email
	 * @param phone
	 * @param password
	 * @param gender
	 * @param type
	 * @param dateBirth
	 * @param regDate
	 * @param idMem
	 */
	
	public Customer(String tax_no, String name, String email, String phone,char[] password, char gender,
			char type,LocalDate dateBirth,LocalDate regDate,int idMem){
		
		super(tax_no, name, email,phone,password,gender, type,dateBirth);
		reg_date = regDate;
		id_mem = idMem;
		//if the user does not have a membership, set it to null.
		//In fact, set membership to null by default and then create it and connect it to its user
		//if present. (SQL query)
		if (id_mem == 0)
		membership = null;
	}
	
	
	
	/**
	 * @param tax_no the tax_no to set
	 */
	public void setTax_no(String tax_no) {
		super.setTax_no(tax_no);
		this.tax_no = tax_no;
	}
	/**
	 * @return the reg_date
	 */
	
	
	public LocalDate getReg_date() {
		return reg_date;
	}
	/**
	 * @param reg_date the reg_date to set
	 */
	public void setReg_date(LocalDate reg_date) {
		this.reg_date = reg_date;
	}


	/**
	 * @return the id_mem
	 */
	public int getId_mem() {
		return id_mem;
	}
	/**
	 * @param id_mem the id_mem to set
	 */
	public void setId_mem(int id_mem) {
		this.id_mem = id_mem;
	}


	/**
	 * @return the membership
	 */
	public Membership getMembership() {
		return membership;
	}
	/**
	 * @param membership the membership to set
	 */
	public void setMembership(Membership membership) {
		this.membership = membership;
	}
	
		
	public boolean equals(Customer cust){
		if (this.tax_no == cust.tax_no)
			return true;
		return false;
	}
	
	
	public String toString(){
		return super.toString()+ " " + reg_date.toString() + " " + ((id_mem == 0)? " ":id_mem);
		
	}
	
	
}
