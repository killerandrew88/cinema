package DataStructure;

import java.sql.Date;
import java.time.LocalDate;

public class Director {
	int id;
	Date birthday;
	String name;
	
	public Director(Date birthday,String name) {
		this.birthday= birthday;
		this.name= name;
	}
	
	public Director(int id,String name, Date birthday ) {
		this.id = id;
		this.birthday = birthday;
		this.name = name;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public void setName(String name) {
		this.name = name;
	}
    
	public int getId() {
		return id;
	}
	
	public Date getDateOfBirth() {
		return birthday;
	}
    
	public String getName() {
		return name;
	}

}
