package DataStructure;
import java.time.LocalDate;

public class Membership {

	private int id,discount;
	private LocalDate acq_date;
	private String title;
	
	/**
	 * Constructor
	 * @param id
	 * @param acq_date
	 * @param title
	 * @param discount
	 */

	public Membership(){
		
	}
	
	public Membership(int id, LocalDate acq_date, String title, int discount) {
		this.id = id;
		this.discount = discount;
		this.acq_date = acq_date;
		this.title = title;
	}

	public Membership(LocalDate acquDate, String title, int discount){
		this.discount = discount;
		this.acq_date = acquDate ;
		this.title = title;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the discount
	 */
	public int getDiscount() {
		return discount;
	}

	/**
	 * @param discount the discount to set
	 */
	public void setDiscount(int discount) {
		this.discount = discount;
	}

	/**
	 * @return the acq_date
	 */
	public LocalDate getAcq_date() {
		return acq_date;
	}

	/**
	 * @param acq_date the acq_date to set
	 */
	public void setAcq_date(LocalDate acq_date) {
		this.acq_date = acq_date;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	public boolean equals(Membership mem){
		if (this.id == mem.getId())
			return true;
		else return false;
	}
	
	public String toString(){
		return id + " " + " " + acq_date.toString() + " " + title + " " + discount;
	}
	
}