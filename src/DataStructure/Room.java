package DataStructure;

public class Room {
	
	int roomNum;
	int numRow;
	int numCol;
	
	public Room (int roomNum, int numRow, int numCol) {
		this.roomNum=roomNum;
		this.numRow=numRow;
		this.numCol=numCol;
	}
	
	public int getRoomNum() {
		return roomNum;
	}
	public void setRoomNum(int roomNum) {
		this.roomNum = roomNum;
	}
	public int getNumRow() {
		return numRow;
	}
	public void setNumRow(int numRow) {
		this.numRow = numRow;
	}
	public int getNumCol() {
		return numCol;
	}
	public void setNumCol(int numCol) {
		this.numCol = numCol;
	}
	
	
	

}
