package DataStructure;

import java.sql.Date;
import java.util.ArrayList;

public class Movie {
	int id;
	String title;
	ArrayList<Director> director;
	String country;
	int duration;
	Date dateOfProduction;
	ArrayList<String> genre;
	
	public Movie() {
		
	}
	
	public Movie(int id, String title, ArrayList<Director> director, String country, int duration, ArrayList<String> genre) {
		this.id = id;
		this.title = title;
		this.director = director;
		this.country = country;
		this.duration = duration;
		this.genre = genre;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public ArrayList<Director> getDirector() {
		return director;
	}


	public void setDirector(ArrayList<Director> director) {
		this.director = director;
	}


	public String getCountry() {
		return country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public int getDuration() {
		return duration;
	}


	public void setDuration(int duration) {
		this.duration = duration;
	}


	public Date getDateOfProduction() {
		return dateOfProduction;
	}


	public void setDateOfProduction(Date dateOfProduction) {
		this.dateOfProduction = dateOfProduction;
	}


	public ArrayList<String> getGenre() {
		return genre;
	}


	public void setGenre(ArrayList<String> genre) {
		this.genre = genre;
	}
	
	
} 

