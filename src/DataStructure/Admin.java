package DataStructure;

import java.time.LocalDate;

public class Admin extends CUser{
	
	private String tax_no; //primary key
	//permission must be 1 or 2
	private int permission;
	
	public Admin(){
		super();
	}
	
	public Admin(String tax_no, String name, String email, String phone,char[] password, char gender,
			char type,LocalDate dateBirth, int perm){
		
		super(tax_no, name, email,phone,password,gender, type,dateBirth);
		
		this.permission = perm;
	}


	/**
	 * @param tax_no the tax_no to set
	 */
	public void setTax_no(String tax_no) {
		super.setTax_no(tax_no);
		this.tax_no = tax_no;
	}

	/**
	 * @return the permission
	 */
	public int getPermission() {
		return permission;
	}

	/**
	 * @param permission the permission to set
	 */
	public void setPermission(int permission) {
		this.permission = permission;
	}
	
	public String toString() {
		return super.toString() + " " + permission;
	}
	
	public boolean equals(Admin adm){
		if (this.tax_no == adm.tax_no)
			return true;
		return false;
}
}

