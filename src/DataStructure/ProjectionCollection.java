package DataStructure;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProjectionCollection {
	ArrayList<Projection> collection;
	
	public ProjectionCollection() {
		collection = new ArrayList<Projection>();
	}
	
	public void loadCollectionFromRs(ResultSet rs) {
		collection = new ArrayList<Projection>();
		if (rs!=null) {
			try {
				while (rs.next()) {
					Movie movie  = new Movie(rs.getInt("movie_id"),rs.getString("title"),null,rs.getString("country"),rs.getInt("duration"),null);
					Room room = new Room(rs.getInt("room_no"),rs.getInt("no_rows"),rs.getInt("no_cols"));
					Projection projection = new Projection (rs.getInt("proj_id"),movie,room,rs.getDouble("price"),rs.getDate("day"),rs.getTime("start_time"),rs.getString("created_by"),rs.getTimestamp("creation_time"),rs.getBoolean("threed_option"));
					collection.add(projection);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<Projection> getCollection() {
		return collection;
	}

	public void setCollection(ArrayList<Projection> collection) {
		this.collection = collection;
	}
	
	

}
