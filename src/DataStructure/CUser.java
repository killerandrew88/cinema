package DataStructure;
import java.time.LocalDate;

public class CUser {
	
	protected String tax_no, name, email,phone_no;
	protected char[] password;
	//gender = f, m; type = a,u;
	protected char gender, type;
	protected LocalDate date_birth;
	
	public CUser(){
		
	}
	
	public CUser(String tax_no, String name, String email, String phone,char[] password, char gender,
			char type,LocalDate dateBirth){
		this.tax_no = tax_no;
		this.name=name;
		this.email = email;
		this.phone_no = phone;
		this.password = password;
		this.gender=gender;
		this.type = type;
		this.date_birth = dateBirth;
	}
	
	/**
	 * @return the tax_no
	 */
	public String getTax_no() {
		return tax_no;
	}
	/**
	 * @param tax_no the tax_no to set
	 */
	public void setTax_no(String tax_no) {
		this.tax_no = tax_no;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the phone_no
	 */
	public String getPhone_no() {
		return phone_no;
	}
	/**
	 * @param phone_no the phone_no to set
	 */
	public void setPhone_no(String phone_no) {
		this.phone_no = phone_no;
	}
	/**
	 * @return the password
	 */
	public char[] getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(char[] password) {
		this.password = password;
	}
	/**
	 * @return the gender
	 */
	public char getGender() {
		return gender;
	}
	/**
	 * @param gender the gender to set
	 */
	public void setGender(char gender) {
		this.gender = gender;
	}
	/**
	 * @return the type
	 */
	public char getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(char type) {
		this.type = type;
	}
	/**
	 * @return the date_birth
	 */
	public LocalDate getDate_birth() {
		return date_birth;
	}
	/**
	 * @param date_birth the date_birth to set
	 */
	public void setDate_birth(LocalDate date_birth) {
		this.date_birth = date_birth;
	}
	
	public String toString(){
		StringBuilder pass = new StringBuilder();
		for (int i=0;i<64;i++){
		pass.append(password[i]);
		}
		
		return tax_no + " " + name + " " + email + " " + phone_no + " " +
				pass.toString() + " " + gender  + " " + type + " " + date_birth.toString();
	}
	
	public boolean equals(CUser user){
		if (this.tax_no == user.tax_no)
			return true;
		return false;
	}
	
	
}
