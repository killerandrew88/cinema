package DataStructure;

import java.time.LocalDateTime;

public class Reservation {
	
//	private Projection proj;
	
private int proj_id;
	
	private String cust_taxno, seat;
	private LocalDateTime date_time;
	
	public Reservation(int proj_id, String cust_taxno, String seat, LocalDateTime date_time) {
		super();
		this.proj_id = proj_id;
		this.cust_taxno = cust_taxno;
		this.seat = seat;
		this.date_time = date_time;
	}


		
	public Reservation(){
		
	}

	/**
	 * @return the proj_id
	 */
	public int getProj_id() {
		return proj_id;
	}

	/**
	 * @param proj_id the proj_id to set
	 */
	public void setProj_id(int proj_id) {
		this.proj_id = proj_id;
	}

	/**
	 * @return the cust_taxno
	 */
	public String getCust_taxno() {
		return cust_taxno;
	}

	/**
	 * @param cust_taxno the cust_taxno to set
	 */
	public void setCust_taxno(String cust_taxno) {
		this.cust_taxno = cust_taxno;
	}

	/**
	 * @return the seat
	 */
	public String getSeat() {
		return seat;
	}

	/**
	 * @param seat the seat to set
	 */
	public void setSeat(String seat) {
		this.seat = seat;
	}

	/**
	 * @return the date_time
	 */
	public LocalDateTime getDate_time() {
		return date_time;
	}

	/**
	 * @param date_time the date_time to set
	 */
	public void setDate_time(LocalDateTime date_time) {
		this.date_time = date_time;
	}
	
	
	
}
