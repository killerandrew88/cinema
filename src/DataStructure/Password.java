package DataStructure;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import Exceptions.InvalidPasswordException;

/**
 * Generates a secure password using the SHA algorithm
 * @author Georgiana
 *
 */

public class Password {

	private String input;
	private char[] passHashcode;
	
	public static void main(String[] args) throws InvalidPasswordException{
		Password p = new Password("password");
		char[] c = p.getPassHashcode();
		for (int i=0;i<64;i++){
			System.out.print(c[i]);
		}
	}
	
	/**
	 * Parameterized constructor. Accepts a String input which will be encrypted immediately
	 * into its secure version. 
	 * @param input The password to secure.
	 * @throws InvalidPasswordException If the password doesn't fulfill the requirements to be valid.
	 */
	public Password(String input) throws InvalidPasswordException {
		this.input = input;
		if (isValidInput(input)){
			this.passHashcode = this.computeHashcode(input);
		}
	}
	
	
	
	/**
	 * Secures the password with SHA-256
	 * @param input to be secured.
	 * @return String Hashcode computed for the input.
	 */
	private char[] computeHashcode(String input) {
		String passwordToHash = input;
		String generatedString;
		char[] generatedPassword = new char[64];
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(passwordToHash.getBytes());
			byte[] bytes = md.digest();
			//This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}

			generatedString = sb.toString();
//			System.out.println(generatedString);
			
			for(int i=0; i<64; i++){
				generatedPassword[i] = generatedString.charAt(i);
			}
			return generatedPassword;
			
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return generatedPassword;
	}

	/**
	 * Checks that the password is valid.
	 * @param input The password that the user has chosen.
	 * @return boolean True if the password is valid,otherwise throws an exception.
	 * @throws InvalidPasswordException If the password doesn't fulfill the requirements to be valid.
	 */
	private boolean isValidInput(String input) throws InvalidPasswordException{
//		if (input.length()<4)
//			throw new InvalidPasswordException("Password must contain at least 4 characters!");
		
		return true;
	}
		
	/**
	 * Gets the secured version of the password.
	 * @return String the hashcode computed for this password.
	 */
	public char[] getPassHashcode(){
		return this.passHashcode;
	}
	
	/**
	 * Sets the secured version of the password(useful when
	 * creating user by reading from database).
	 * @param hashC the hashcode of the password.
	 */
	public void setPassHashcode(char[] hashC){
		this.passHashcode = hashC;
		
	}
	/**
	 * Gets the String representation of the password by returning its secure version.
	 * @return String The password string representation, which is its hashcode.
	 */
//	@Override 
//	public String toString(){
//		return this.passHashcode;
//	}
//	
	/**
	 * Checks if two passwords are equal by comparing 
	 * their secured hashcodes.
	 * @return boolean True if the passwords have the same hashcode, false otherwise.
	 */
	@Override
	public boolean equals(Object pass) {

		if (pass instanceof Password) {
			Password other = (Password) pass;
			boolean same = true;
			for (int i = 0; i < 64; i++) {
				if (this.passHashcode[i] == other.passHashcode[i]) {
					//
				} else
					same = false;
			}
			return same;
		} else
			return false;
	}

}