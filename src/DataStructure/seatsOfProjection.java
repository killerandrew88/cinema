package DataStructure;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import Utilities.DBManager;

public class seatsOfProjection {
	int proj_id;
	ArrayList <Seat> seats;
	Room room;
	DBManager dbm;
	ArrayList <Seat> newReservation;

	float projTicketCost;
	
	public ArrayList<Seat> getSeats() {
		return seats;
	}

	
	public Room getRoom() {
		return room;
	}


	public void setRoom(Room room) {
		this.room = room;
	}
	
	public ArrayList<Seat> getNewReservation() {
		return newReservation;
	}


	public void setNewReservation(ArrayList<Seat> newReservation) {
		this.newReservation = newReservation;
	}


	public seatsOfProjection(int proj_id) {
		this.proj_id = proj_id;
		this.room = null;
		String query = "SELECT r.seat , p.room_no , room.no_rows , room.no_cols, p.price\r\n" + 
				"FROM reservation as r,projection as p,room\r\n" + 
				"WHERE r.proj_id = p.proj_id and p.room_no = room.room_no and r.proj_id ="+proj_id+";";
		
		String queryAux = "SELECT p.room_no , r.no_rows, r.no_cols , p.price" + 
				" FROM projection p , room r"+ 
				" WHERE p.room_no = r.room_no and p.proj_id ="+proj_id+";";
		System.out.println(queryAux);
		
		seats = new ArrayList<Seat>();
		dbm = new DBManager();
		dbm.query(query);
		this.fillDataFromResultSet(dbm.getRs());
		dbm.closeSets();
		if (this.room == null) {;
			//no reservation exist for this projection, another type of query has to be made
			System.out.println("No reservations for projection");
			dbm.query(queryAux);
			loadRoomData(dbm.getRs());
			dbm.closeSets();
		}				
		this.createAllSeats();
		Collections.sort(this.seats);
		newReservation = new ArrayList<Seat>();
		
	}
	
	private void loadRoomData(ResultSet rs) {
		if (rs!=null) {
			try {
				while(rs.next()) {
					this.room = new Room(rs.getInt(1),rs.getInt(2),rs.getInt(3));
					this.projTicketCost = rs.getFloat(4);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private void fillDataFromResultSet(ResultSet rs) {
		String[] split = null;
		int no,rows,cols;
		int count =0;
		if (rs!=null) {
			try {
				while (rs.next()) {
					split = rs.getString(1).split("-");
					int row = Integer.parseInt(split[0]);
					int col = Integer.parseInt(split[1]);
					this.seats.add(new Seat("",row,col,true));
					if (count == 0) {
						this.room = new Room(rs.getInt(2),rs.getInt(3),rs.getInt(4));
					}
					this.projTicketCost = rs.getFloat(5);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	private void createAllSeats() {
		int r;
		int c;
		for (r=0;r<this.room.getNumRow();r++) {
			for (c=0;c<this.room.getNumCol();c++) {
				Seat s = new Seat("",r,c,false);
				if (!seats.contains(s))
					seats.add(s);
			}
		}
	}
	public float getReservationCost() {
		return this.newReservation.size() * this.projTicketCost;
	}


	public float getProjTicketCost() {
		return projTicketCost;
	}


	public void setProjTicketCost(float projTicketCost) {
		this.projTicketCost = projTicketCost;
	}
	
	/** performs INSERT query in db*/
	public void performReservation(String tax_no) {
		if (this.getNewReservation().size()>0) {
			int c = 0;
			String query ="INSERT INTO reservation VALUES";
			for (Seat s : this.newReservation) {
				String seatno = ""+s.getRow()+"-"+s.getCol();
				LocalDateTime a = LocalDateTime.now();
				String datetime;
				query += "("+this.proj_id+",'"+tax_no+"','"+seatno+"','"+a+"')";
				c++;
				if (c<this.getNewReservation().size()) {
					query+=",";
				}
			}
			query += ";";
			if (dbm.updateSql(query)==true) {
				System.out.println("Update success");
			};
			
			System.out.println(query);
		}
	}
	
	
	/**private void createRoomFromProjection(int proj_id) {
		String query = "SELECT r.room_no,r.no_rows,r.no_cols\r\n" + 
				"FROM projection as p,room as r\r\n" + 
				"WHERE p.proj_id = "+proj_id+" and p.room_no = r.room_no;";
		dbm.query(query);
		room = createRoomFromResultSet(dbm.getRs());
		dbm.closeSets();
		if (room == null) {
			System.out.println("Cannot create a room for this projection , error occurred");
			room = new Room(999,10,10);
		}
	}
	private Room createRoomFromResultSet(ResultSet rs) {
		int no,rows,cols;
		Room room = null;
		if (rs == null) {
			return null;
		}else {
			try {
				while(rs.next()) {
					no = rs.getInt(1);
					rows = rs.getInt(2);
					cols = rs.getInt(3);
					room = new Room(no,rows,cols);
				}
			} catch (SQLException e) {			
				e.printStackTrace();
			}
			return room;
		}
	}*/







}


