package DataStructure;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;

public class Seat extends JButton implements Comparable {
	int row;
	int col;
	boolean taken;
	
	public void setRow(int row) {
		this.row = row;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public void setTaken(boolean taken) {
		this.taken = taken;
	}

	public int getRow() {
		return row;
	}

	public int getCol() {
		return col;
	}

	public boolean isTaken() {
		return taken;
	}

	public String toString() {
		return this.row+"-"+this.col;
	}
	
	public Seat() {
		
	}

	public Seat(Icon arg0) {
		super(arg0);

	}

	public Seat(String arg0,int row,int col,boolean taken) {
		super(arg0);
		this.row = row;
		this.col = col;
		this.taken = taken;
		if (this.taken) {
			this.setBackground(Color.red);
		}
		else {
			this.setBackground(Color.GRAY);
		}
		this.createActonListener();
	}
	public Seat(String arg0,int row,int col) {
		super(arg0);
		this.row = row;
		this.col = col;
		this.taken = false;
		this.setBackground(Color.GRAY);
		this.createActonListener();
	}

	public Seat(Action arg0) {
		super(arg0);
		
	}

	public Seat(String arg0, Icon arg1) {
		super(arg0, arg1);
		
	}
	private void setDimensions() {
		/*this.setPreferredSize(new Dimension(5,5));
		this.setSize(new Dimension(5,5));*/
	}
	
	/**
	 * changes state from free to reserved and from reserved to free.. used to select spot by user.
	 */
	public void changeState() {
		this.taken = !this.taken;
		if (taken) {
			this.setBackground(Color.GREEN);
		}else {
			this.setBackground(Color.GRAY);
		}
	}
	private void createActonListener() {
		this.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Seat s = (Seat) arg0.getSource();
				if(!(s.getBackground()== Color.red)) {
					s.changeState();
				}else {
					JOptionPane.showMessageDialog(null, "Cannot chose already reserved seats");
				}
				
			}
			
		});
	}

	@Override
	public boolean equals(Object obj) {		
		if (obj instanceof Seat) {
			Seat s = (Seat) obj;
			if (this.col == s.col && this.row == s.row)
				return true;
			else 
				return false;
		}else {
			return false;
		}
		
	}
	@Override
	public int compareTo(Object obj){
		Seat arg = (Seat) obj;
		if (this.equals(arg))
			return 0;
		if(this.row<arg.row) {
			return -1;
		}
		else if (this.row==arg.row) {
			if (this.col<arg.col) {
				return -1;
			}else
				return 1;
		}
		else if(this.row>arg.row) {
			return 1;
		}
		return 4;
	}
	
	

}
