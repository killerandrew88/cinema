package DataStructure;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

public class Proj_Reserv_Movie {
	
    private int proj_id,movieId,duration,roomId;
	private String cust_taxno, seat,title,country,proj_createdBy,Email;
	Timestamp reser_date_time; //cambiato da localDateTime
	private Timestamp proj_creationDateTime;
	private Movie movie;
	private double price;
	private Date dateOfProduction; // Date and not Local date due to the resultSet
	private Date proj_day;
	private Time proj_startTime;
	private Boolean threeD_Option;
	private Director director;
	
	
	public Proj_Reserv_Movie() {
		
	}
	
	

	public Proj_Reserv_Movie(int proj_id, int movieId, int duration, int roomId, String cust_taxno, String seat,
			String title, String country, String proj_createdBy, Timestamp reser_date_time,
			Timestamp proj_creationDateTime, Movie movie, double price, Date dateOfProduction, Date proj_day,
			Time proj_startTime, Boolean threeD_Option, Director director) {
		super();
		this.proj_id = proj_id;
		this.movieId = movieId;
		this.duration = duration;
		this.roomId = roomId;
		this.cust_taxno = cust_taxno;
		this.seat = seat;
		this.title = title;
		this.country = country;
		this.proj_createdBy = proj_createdBy;
		this.reser_date_time = reser_date_time;
		this.proj_creationDateTime = proj_creationDateTime;
		this.movie = movie;
		this.price = price;
		this.dateOfProduction = dateOfProduction;
		this.proj_day = proj_day;
		this.proj_startTime = proj_startTime;
		this.threeD_Option = threeD_Option;
		this.director = director;
	}



	public int getProj_id() {
		return proj_id;
	}

	public void setProj_id(int proj_id) {
		this.proj_id = proj_id;
	}


	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		this.Email = email;
	}
	
	public int getMovieId() {
		return movieId;
	}

	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public int getRoomId() {
		return roomId;
	}

	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	public String getCust_taxno() {
		return cust_taxno;
	}

	public void setCust_taxno(String cust_taxno) {
		this.cust_taxno = cust_taxno;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getProj_createdBy() {
		return proj_createdBy;
	}

	public void setProj_createdBy(String proj_createdBy) {
		this.proj_createdBy = proj_createdBy;
	}

	public Timestamp getReser_date_time() {
		return reser_date_time;
	}

	public void setReser_date_time(Timestamp reser_date_time) {
		this.reser_date_time = reser_date_time;
	}

	public Timestamp getProj_creationDateTime() {
		return proj_creationDateTime;
	}

	public void setProj_creationDateTime(Timestamp timestamp) {
		this.proj_creationDateTime = timestamp;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDateOfProduction() {
		return dateOfProduction;
	}

	public void setDateOfProduction(Date dateOfProduction) {
		this.dateOfProduction = dateOfProduction;
	}

	public Date getProj_day() {
		return proj_day;
	}

	public void setProj_day(Date date) {
		this.proj_day = date;
	}

	public Time getProj_startTime() {
		return proj_startTime;
	}

	public void setProj_startTime(Time time) {
		this.proj_startTime = time;
	}

	public Boolean getThreeD_Option() {
		return threeD_Option;
	}

	public void setThreeD_Option(Boolean threeD_Option) {
		this.threeD_Option = threeD_Option;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}
	
	
	
	
	

	

}
