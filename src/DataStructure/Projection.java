package DataStructure;


import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.*;

public class Projection {

	int projID;
	Movie movie;
	Room room;
	double price;
	Date day;
	Time startTime;
	String createdBy;
	Timestamp creationDateTime;
	Boolean threeD_Option;
	
	
	
	public Projection(int projID, Movie movie, Room room, double price, Date date, Time time,
			String createdBy, Timestamp timestamp, Boolean threeD_Option) {
		super();
		this.projID = projID;
		this.movie = movie;
		this.room = room;
		this.price = price;
		this.day = date;
		this.startTime = time;
		this.createdBy = createdBy;
		this.creationDateTime = timestamp;
		this.threeD_Option = threeD_Option;
	}



	public void setProjID(int projID) {
		this.projID = projID;
	}



	public void setMovie(Movie movie) {
		this.movie = movie;
	}



	public void setRoom(Room room) {
		this.room = room;
	}



	public void setPrice(double price) {
		this.price = price;
	}



	public void setDay(Date day) {
		this.day = day;
	}



	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public void setCreationDateTime(Timestamp creationDateTime) {
		this.creationDateTime = creationDateTime;
	}



	public void setThreeD_Option(Boolean threeD_Option) {
		this.threeD_Option = threeD_Option;
	}



	public int getProjID() {
		return projID;
	}



	public Movie getMovie() {
		return movie;
	}



	public Room getRoom() {
		return room;
	}



	public double getPrice() {
		return price;
	}



	public Date getDay() {
		return day;
	}



	public Time getStartTime() {
		return startTime;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public Timestamp getCreationDateTime() {
		return creationDateTime;
	}



	public Boolean getThreeD_Option() {
		return threeD_Option;
	}
	

	

	
}