package DataStructure;

import java.util.ArrayList;

import Utilities.UserCollection;

public class ActiveUser {

	private boolean admin;
	private Customer activeCust;
	private Admin activeAd;
	private CUser user;
	
	public static void main(String[] args){
		UserCollection coll = new UserCollection();
		ArrayList<Admin> admins = coll.getAdminList(coll.loadAllAdmins());
		ActiveUser au = new ActiveUser(admins.get(0));
		System.out.println(au.getActiveAdmin());
		System.out.println(au.getUser());
		
		ArrayList<Customer> custs = coll.getCustomerList(coll.loadAllCustomers());
		au = new ActiveUser(custs.get(0));
		System.out.println(au.getActiveCust());
		System.out.println(au.getUser());
	}
	
	public <U extends CUser> ActiveUser(U u){
		user = u;
		inspect(user);
		
	}
	
	public <U extends CUser> void inspect(U u){
		
		if (u.getType() == 'a' || u.getType() == 'A'){
			
			admin = true;
			if (u instanceof Admin){
				setActiveAdmin((Admin) u);
			}
			
		}else if (u.getType() == 'u' || u.getType() == 'U'){

			admin = false;
			if (u instanceof Customer){
				setActiveCust((Customer) u);
			}
		}
		
		else System.out.println("Error in class ActiveUser: Could not get type of user!");
	
	}

	/**
	 * @return the activeCust
	 */
	public Customer getActiveCust() {
		return activeCust;
	}

	/**
	 * @param activeCust the activeCust to set
	 */
	public void setActiveCust(Customer activeCust) {
		this.activeCust = activeCust;
	}

	/**
	 * @return the activeAd
	 */
	public Admin getActiveAdmin() {
		return activeAd;
	}

	/**
	 * @param activeAd the activeAd to set
	 */
	public void setActiveAdmin(Admin activeAd) {
		this.activeAd = activeAd;
	}

	/**
	 * @return the admin
	 */
	public boolean isAdmin() {
		return admin;
	}

	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	/**
	 * @return the user
	 */
	public CUser getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(CUser user) {
		this.user = user;
	}
	
	public String toString(){
		
		if (isAdmin()){
			return this.activeAd.toString();
		} else return this.activeCust.toString();
		
		
	}
}
