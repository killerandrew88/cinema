package Exceptions;

/**
 * @author Georgiana
 *	Checked exception, subclasses Exception. Launched when a Password
 *	is not valid. 
 */

public class InvalidPasswordException extends Exception{
	
	private String message;

	/**
	 * Constructor of the Exception. 
	 * @param mess the message that specifies more about what has caused the exception.
	 */
	public InvalidPasswordException(String mess){
		this.message = mess;
	}
	
	/**
	 * Gets the message of the exception.
	 * @return String The message passed as parameter to the constructor.
	 */
	public String getMessage(){
		return this.message;
	}

}
