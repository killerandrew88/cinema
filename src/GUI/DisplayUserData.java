package GUI;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import DataStructure.CUser;
import DataStructure.Password;
import Exceptions.InvalidPasswordException;
import Utilities.UserCollection;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;

public class DisplayUserData extends JPanel{
	
	private CUser user;
	private JLabel tax,name,email,phone,gender;
	private JButton changePass,changeInfo;
	private JPasswordField passwordField,oldPass;
	
	public DisplayUserData(CUser user) {
		super();
		setFont(new Font("Tahoma", Font.PLAIN, 15));
		this.user = user;
		
		setSize(229,350);
		setLayout(null);
		phone = new JLabel("Phone: " + user.getPhone_no());
		phone.setFont(new Font("Tahoma", Font.PLAIN, 15));
		phone.setBounds(21, 109, 200, 30);
		add(phone);
		
		email = new JLabel("Email: " + user.getEmail());
		email.setFont(new Font("Tahoma", Font.PLAIN, 15));
		email.setBounds(21,73,200,30);
		add(email);
		
		name = new JLabel("Name: " + user.getName());
		name.setFont(new Font("Tahoma", Font.PLAIN, 15));
		name.setBounds(21,41,200,30);
		add(name);
		
		tax = new JLabel("Tax number: " + user.getTax_no());
		tax.setFont(new Font("Tahoma", Font.PLAIN, 15));
		tax.setBounds(21,11,200,30);
		add(tax);
		
		gender = new JLabel("Gender: " + user.getGender());
		gender.setFont(new Font("Tahoma", Font.PLAIN, 15));
		gender.setBounds(21,138,200,30);
		add(gender);
		
//		changeInfo = new JButton("Change information");
//		changeInfo.setFont(new Font("Tahoma", Font.PLAIN, 13));
//		changeInfo.setBounds(21,207,150,30);
//		add(changeInfo);
		
		changePass = new JButton("Change password");
		changePass.setFont(new Font("Tahoma", Font.PLAIN, 13));
		changePass.setBounds(21,260,150,30);
		changePass.addActionListener(new ChangePasswordListener());
		add(changePass);
	
		
		
		
	}

	
	private void showPasswordFrame(String userTax){
		JFrame pFrame = new JFrame("Change password");
		pFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JPanel panel= new JPanel();
		panel.setLayout(null);
		JLabel lab1 = new JLabel("Old password:");
		lab1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lab1.setBounds(44,46,90,30);
		panel.add(lab1);
		
		oldPass = new JPasswordField();
		oldPass.setBounds(169,52,124,20);
		panel.add(oldPass);
		
		JLabel lab2 = new JLabel("New password: ");
		
		panel.add(lab2);
		
		JLabel lblNewPassword = new JLabel("New password:");
		lblNewPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNewPassword.setBounds(44, 126, 90, 14);
		panel.add(lblNewPassword);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(169, 124, 124, 20);
		panel.add(passwordField);
		
		JButton btnSave = new JButton("Save");
		btnSave.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnSave.setBounds(114, 204, 89, 23);
		btnSave.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				String oldPasswordInput = new String(oldPass.getPassword());
				String newPasswordInput = new String(passwordField.getPassword());
				
				Password oldPassword = null,newPassword = null;
				try {
					if (!oldPasswordInput.equals("")) {
						oldPassword = new Password(oldPasswordInput);
					} else
						JOptionPane.showMessageDialog(null, "Please insert your old password!");
				} catch (InvalidPasswordException e1) {

				}
				try {
					if (!newPasswordInput.equals("")) {
						newPassword = new Password(newPasswordInput);
					} else
						JOptionPane.showMessageDialog(null, "Please insert your new password!");
				} catch (InvalidPasswordException e1) {

				}
				if (oldPassword!=null && newPassword!= null){
					//check if oldPassword corresponds to the one in db of user
					Password check;
					try {
						check = new Password("");
						check.setPassHashcode(user.getPassword());
						boolean same = check.equals(oldPassword);
						if(same) {
							UserCollection userColl = new UserCollection();
							userColl.updatePassword(user.getTax_no(), newPassword.getPassHashcode());
							oldPass.setText("");
							passwordField.setText("");
						} else {
							JOptionPane.showMessageDialog(null, "Your old password is not correct!");
						}
						} catch (InvalidPasswordException e1) {
						
					}
					
					//if yes, save new password in db
				}
				
			}
			
		});
		panel.add(btnSave);
		
		pFrame.getContentPane().add(panel);
		
		pFrame.setMinimumSize(new Dimension(400,350));
		pFrame.pack();
		pFrame.setLocationRelativeTo(null);
		pFrame.setMinimumSize(new Dimension(400,350));
		pFrame.setVisible(true);
		
		pFrame.setFont(new Font("Tahoma", Font.PLAIN, 14));
		pFrame.setSize(new Dimension(310, 280));
		pFrame.getContentPane().setLayout(null);
		
	}
	
	private class ChangePasswordListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
		
			showPasswordFrame(user.getTax_no());
			
		}
		
	}
}
