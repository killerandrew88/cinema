package GUI;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.swing.JTextField;
import javax.swing.event.ListDataListener;

import DataStructure.ActiveUser;
import DataStructure.Admin;
import DataStructure.CUser;
import DataStructure.Customer;
import DataStructure.Membership;
import DataStructure.Password;
import Exceptions.InvalidPasswordException;
import Utilities.MembershipCollection;
import Utilities.UserCollection;

//import DateChooser.RangeModel;

import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

public class RegisterFrame extends JFrame {
	private ButtonListener listener;
	private JButton btnRegister;
	private UserCollection userColl;
	private LoginPanel logInPane;
	private JPanel panel;
	//I need this as parameter to loginPanel
	private JFrame sFrame;
	
	//I need this as parameter for adding users
	private Admin newAdmin;
	private JRadioButton perm1,perm2;
	private ButtonGroup group;
	
	private Customer newCustomer;
	
	private Registration registra;
	private JLabel lblMembership;
	
	private MembershipCollection memColl;
	
	JCheckBox chckbxGrantMembership;
	/**
	 * @wbp.parser.constructor
	 */
	public RegisterFrame(Admin admin){
		super();
		newAdmin = admin;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setJMenuBar(MenuCreator.createMenuBar());
		MenuCreator.setLogoutMenu(this);
		
		setMinimumSize(new Dimension(750,600));
		pack();
		setLocationRelativeTo(null);
    	setMinimumSize(new Dimension(750,600));
		setVisible(true);
		
		
		commonInterface();
		interfaceAdmin();
	
	}
	
	
	
	public RegisterFrame(Customer customer){
		super();
		newCustomer = customer;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setJMenuBar(MenuCreator.createMenuBar());
		MenuCreator.setLogoutMenu(this);
		
		setMinimumSize(new Dimension(750,600));
		pack();
		setLocationRelativeTo(null);
    	setMinimumSize(new Dimension(750,600));
		setVisible(true);
		
		commonInterface();
		interfaceCustomer();
		
	}
	/**
	 * Create the panel.
	 * @wbp.parser.constructor
	 */
	public RegisterFrame(JFrame frame) {
		super();
//		setSize()
		sFrame = frame;

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setJMenuBar(MenuCreator.createMenuBar());
		MenuCreator.setLogoutMenu(this);
		
		setMinimumSize(new Dimension(750,600));
		pack();
		setLocationRelativeTo(null);
//    	setMinimumSize(new Dimension(750,600));
		setVisible(true);
		
		
		
		commonInterface();
	}
	
	private void commonInterface(){
		
		userColl = new UserCollection();
		memColl = new MembershipCollection();
		listener = new ButtonListener();
		getContentPane().setLayout(null);
		setMinimumSize(new Dimension(700,640));
		
		panel = new JPanel();
		panel.setBounds(20, 45, 479, 518);
		getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblRegistrationForm = new JLabel("REGISTRATION FORM");
		lblRegistrationForm.setBounds(20, 11, 185, 23);
		getContentPane().add(lblRegistrationForm);
		lblRegistrationForm.setFont(new Font("Tahoma", Font.PLAIN, 15));
		
		registra = new Registration();
		registra.setBounds(0, 0, 470, 400);
		panel.add(registra);
		
		btnRegister = new JButton("REGISTER");
		btnRegister.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnRegister.setBounds(0,475,200,30);
		btnRegister.addActionListener(listener);
		panel.add(btnRegister);
		
		
	}
	
	private void interfaceCustomer(){
//		lblMembership = new JLabel("Membership:");
//		lblMembership.setFont(new Font("Tahoma", Font.PLAIN, 14));
//		lblMembership.setBounds(0, 411, 109, 30);
//		panel.add(lblMembership);
//		
//		chckbxGrantMembership = new JCheckBox("Grant membership");
//		chckbxGrantMembership.setBounds(158, 417, 143, 23);
//		chckbxGrantMembership.addActionListener(listener);
//		panel.add(chckbxGrantMembership);
	}
	
	
	
	private void interfaceAdmin(){
		JLabel lblPermission = new JLabel("Permission:");
		lblPermission.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPermission.setBounds(0, 411, 117, 24);
		panel.add(lblPermission);
		
		perm1 = new JRadioButton("1");
		perm1.setBounds(160, 407, 109, 23);
		perm1.setSelected(true);
		panel.add(perm1);
		
		perm2 = new JRadioButton("2");
		perm2 .setBounds(301, 407, 109, 23);
		panel.add(perm2);
		
		group = new ButtonGroup();
		group.add(perm1);
		group.add(perm2);
	}
	
	private class ButtonListener implements ActionListener{
		
		CUser user;
		Admin adm;
		Customer cust;
		String tax,name,email,phone,pass,passCheck;
		Password password, passwordCheck;
		char gen;
		LocalDate dateBirth,reg_date;
		int permission;
		
		private void readInput() {
			
			tax = registra.getInputTax();
			name = registra.getInputName();
			email = registra.getInputEmail();
			phone = registra.getInputPhone();
			pass = registra.getPassword();
			passCheck = registra.getPasswordCheck();
			
			password = null;
			passwordCheck = null;
		
			try {
				if (!pass.equals("")){
				password = new Password(pass);
				} else {
					JOptionPane.showMessageDialog(null,"Please insert a password!");
				}
			} catch (InvalidPasswordException e1) {

			} 
			try {
				passwordCheck = new Password(passCheck);
			} catch (InvalidPasswordException e1) {
				
			}
			gen = 'U';
			
			dateBirth = registra.getInputDateBirth();
			
			Date reg = new Date(System.currentTimeMillis());
			reg_date = reg.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			//read permission of admin
			if (newAdmin!= null){
				if (perm1.isSelected())
					permission = 1;
				 else
					permission = 2;
			}
			
		}
		
		// Checks that user has written the check password correctly.
		private void performPassCheck(){
			if (password != null) {
				if (passwordCheck.equals("") || !password.equals(passwordCheck)) {
					JOptionPane.showMessageDialog(null,
							"Wrong password!\nPlease insert the right password\nin the check password field!");
					registra.setPasswordField("");
					passwordCheck = null;
				}
			}
		}
		
		private boolean createAndSaveCust(){
			
			
			// I assume only a customer can register this way
			cust = new Customer(tax, name, email, phone, password.getPassHashcode(), gen, 'U', dateBirth, reg_date,
					0);
			ActiveUser actu = new ActiveUser(cust);
			System.out.println(cust);
			
			boolean ok = userColl.addUser(actu);
			
			if (ok) {
				System.out.println("HURRAYYY");
				JOptionPane.showMessageDialog(null, "You have registered successfully!\n"
						+ "Now please log in.");
				logInPane = new LoginPanel(sFrame);
				setVisible(false);
				sFrame.setVisible(true);
				logInPane.setBounds(1,1,500,400);
				removeAll();
				add(logInPane);
				repaint();
				
			} else {
				JOptionPane.showMessageDialog(null, "Your registration could not be completed.\n"
						+ "Please try again.");
				registra.setInputFields("");
			}
			
			return ok;
		}
		
		private boolean saveCustomer(){
			newCustomer.setTax_no(tax);
			newCustomer.setName(name);
			newCustomer.setEmail(email);
			newCustomer.setPhone_no(phone);
			newCustomer.setDate_birth(dateBirth);
			newCustomer.setGender(gen);
			newCustomer.setPassword(password.getPassHashcode());
			newCustomer.setType('U');
			newCustomer.setReg_date(reg_date);
			newCustomer.setId_mem(0);
			
			ActiveUser nAUc = new ActiveUser(newCustomer);
			boolean ok = userColl.addUser(nAUc);
			
			if (ok){
				registra.setInputFields("");
			}else {
				JOptionPane.showMessageDialog(null, "Your registration could not be completed.\n"
						+ "Please try again.");
				registra.setInputFields("");
			}
			return ok;
		}
		
		private boolean saveAdmin(){
			newAdmin.setTax_no(tax);
			newAdmin.setName(name);
			newAdmin.setEmail(email);
			newAdmin.setPhone_no(phone);
			newAdmin.setDate_birth(dateBirth);
			newAdmin.setGender(gen);
			newAdmin.setPassword(password.getPassHashcode());
			newAdmin.setType('A');
			newAdmin.setPermission(permission);
			
			ActiveUser nAU = new ActiveUser(newAdmin);
			boolean ok = userColl.addUser(nAU);
			if (ok){
				registra.setInputFields("");
			}else {
				JOptionPane.showMessageDialog(null, "Your registration could not be completed.\n"
						+ "Please try again.");
				registra.setInputFields("");
			}
			return ok;
		}
		/**
		 * Reads input fields and checks whether the inputs are valid
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
		
			
			this.readInput();
			
			this.performPassCheck();
	
			if (!tax.equals("") && !name.equals("") && 
					!email.equals("") && !phone.equals("")
					&&password!=null
					&& passwordCheck != null ) {
				
				registra.setActionListener(this);
				if (registra.rdbtnM.isSelected()){
					gen = 'M';
				} else if (registra.rdbtnF.isSelected()){
					gen = 'F';
				} else {
					JOptionPane.showMessageDialog(null,"Please select the gender!");
				}
				
				if (gen!= 'U'){
				
				//We are in the case a new customer wants to register
				if(sFrame!=null){
				
				boolean done = this.createAndSaveCust();
				
				} else if (newAdmin!= null){
					
					saveAdmin();
					JOptionPane.showMessageDialog(null,"Insertion completed!");
//					dispose();
					
				} else if (newCustomer != null){
					
					saveCustomer();
					JOptionPane.showMessageDialog(null,"Insertion completed!");
//					dispose();
				}
				}
			} else {
				JOptionPane.showMessageDialog(null, "Please insert all data!");
			}

		}
		
	}
}
	
