package GUI;

import javax.swing.JFrame;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.toedter.calendar.JDateChooser;

import DataStructure.Director;
import Utilities.MovieCollection;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JButton;

public class NewMovieFrame extends JFrame  {
	private JTextField titleField, nameField, dateOfBirthField;
	private JTextField durationField;
	private JTextField countryField;
	private MovieCollection movieCollection= new MovieCollection();
	private HashSet<String> genreList,directorList,selectedGenreList,selectedDirectorList;
	private DefaultListModel<String> genreModel,selectedGenresModel, directorModel, selectedDirectorsModel;
	private JButton addGenrebtn,removeGenrebtn,addDirectorbtn,removeDirectorbtn, newDirectorbtn;
	private JList<String> genre,selectedGenres, director, selectedDirectors;
	private ListMultimap<Integer,String> directorNameId;
	private JDateChooser dateChooser;
	
	
	public NewMovieFrame() {
		setSize(new Dimension(600, 700));
		setPreferredSize(new Dimension(600, 700));
		setVisible(true);
		getContentPane().setPreferredSize(new Dimension(600, 700));
		
		JLabel lblInsertTitle = new JLabel("INSERT TITLE: ");
		
		titleField = new JTextField();
		titleField.setColumns(10);
		
		JLabel lblInsertDuration = new JLabel("INSERT DURATION");
		
		durationField = new JTextField();
		durationField.setColumns(10);
		
		JLabel lblInsertCountry = new JLabel("INSERT COUNTRY:");
		
		countryField = new JTextField();
		countryField.setColumns(10);
		
		JLabel lblSelectGenre = new JLabel("SELECT GENRE");
		
		JLabel lblGenresSelected = new JLabel("GENRES SELECTED");
		
		
		// genres List
		genreModel= new DefaultListModel<String>();
		genre = new JList<String>(genreModel);
		genre.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		genre.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		genre.setPreferredSize(new Dimension(100, 100));
		genre.setSize(new Dimension(100, 100));
		genreList = getHashSet(movieCollection.allGenreQuery(),2);
		fillList(genreModel,genreList);
		
		selectedGenreList= new HashSet<String>();
		 
		//double click mouseListener
		genre.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		    	HashSet <String> selectedGenre= new HashSet <String>();
		        JList<String> list = (JList<String>)evt.getSource();
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		            int index = list.locationToIndex(evt.getPoint());
		           
                    selectedGenreList.add(list.getSelectedValue());
                    selectedGenre.add(list.getSelectedValue());
		            selectedGenresModel.addElement(list.getSelectedValue());
		            genreModel.removeElement(list.getSelectedValue());
		        } 
		    }
		});
		
		//selected genres list
		 selectedGenresModel= new DefaultListModel<String>();
	    selectedGenres = new JList<String>(selectedGenresModel);
		selectedGenres.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		selectedGenres.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	
		//double click mouseListener
		selectedGenres.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		    	HashSet <String> selectedGenre= new HashSet <String>();
		        JList<String> list = (JList<String>)evt.getSource();
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		            int index = list.locationToIndex(evt.getPoint());
		           
		            selectedGenreList.removeAll(selectedGenres.getSelectedValuesList());
					fillList(genreModel,selectedGenres.getSelectedValuesList());
					removeElementsFromList(selectedGenresModel,selectedGenres.getSelectedValuesList());
		        } 
		    }
		});
		
			
		
		
		
		
		JLabel lblSelectDateOf = new JLabel("SELECT DATE OF PRODUCTION:");
		
		 dateChooser = new JDateChooser();
		 LocalDate now= LocalDate.now();
		 Date date = java.sql.Date.valueOf(now);
		 dateChooser.setDate(date);
		
		JLabel lblSelectDirector = new JLabel("SELECT DIRECTOR");
		
		
		// directors List
		directorModel= new DefaultListModel<String>();
		director = new JList<String>(directorModel);
		director.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		directorList = getHashSet(movieCollection.allDirectorQuery(),1);
		System.out.println(directorList.toString());
		fillList(directorModel,directorList);
		directorNameId= movieCollection.getMultiMap(movieCollection.DirectorNameIdQuert());
		JScrollPane scroll = new JScrollPane(director);
		
		selectedDirectorList = new HashSet<String>();
		
		director.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		    	HashSet <String> selectedDirector= new HashSet <String>();
		        JList<String> list = (JList<String>)evt.getSource();
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		            int index = list.locationToIndex(evt.getPoint());
		           
                    selectedDirectorList.add(list.getSelectedValue());
                    selectedDirector.add(list.getSelectedValue());
		            selectedDirectorsModel.addElement(list.getSelectedValue());
		            directorModel.removeElement(list.getSelectedValue());
		        } 
		    }
		});
		
		
		selectedDirectorsModel= new DefaultListModel<String>();
		selectedDirectors = new JList<String>(selectedDirectorsModel);
		selectedDirectors.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		JScrollPane scrollSD = new JScrollPane(selectedDirectors);
		
		selectedDirectors.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        JList<String> list = (JList<String>)evt.getSource();
		        if (evt.getClickCount() == 2) {

		            // Double-click detected
		            int index = list.locationToIndex(evt.getPoint());
		           
		            selectedDirectorList.removeAll(selectedDirectors.getSelectedValuesList());
					fillList(directorModel,selectedDirectors.getSelectedValuesList());
					removeElementsFromList(selectedDirectorsModel,selectedDirectors.getSelectedValuesList());
		        } 
		    }
		});
		
		JButton btnCreateNewMovie = new JButton("CREATE NEW MOVIE");
		btnCreateNewMovie.addActionListener(newMovieAL);
		
		addGenrebtn = new JButton("ADD");
		addGenrebtn.addActionListener(ButtonListener);
		
		removeGenrebtn = new JButton("REMOVE");
		removeGenrebtn.addActionListener(ButtonListener);
		
		
		addDirectorbtn = new JButton("ADD");
		addDirectorbtn.addActionListener(ButtonListener);
		
		newDirectorbtn = new JButton("NEW DIRECTOR");
		newDirectorbtn.addActionListener(newDirector);
		
		removeDirectorbtn = new JButton("REMOVE");
		removeDirectorbtn.addActionListener(ButtonListener);
		
		JLabel lblSelectedDirector = new JLabel("SELECTED DIRECTOR");
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(21)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblInsertCountry)
									.addGap(18)
									.addComponent(countryField, GroupLayout.PREFERRED_SIZE, 157, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(lblInsertTitle)
											.addGap(37))
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(lblInsertDuration)
											.addGap(18)))
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(titleField, GroupLayout.PREFERRED_SIZE, 264, GroupLayout.PREFERRED_SIZE)
										.addComponent(durationField, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)))
								.addComponent(lblSelectDirector)
								.addGroup(groupLayout.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
											.addComponent(lblSelectDateOf)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE))
										.addGroup(groupLayout.createSequentialGroup()
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lblSelectGenre)
												.addComponent(genre, GroupLayout.PREFERRED_SIZE, 143, GroupLayout.PREFERRED_SIZE)
												.addComponent(addGenrebtn)
												.addComponent(scroll, GroupLayout.PREFERRED_SIZE, 199, GroupLayout.PREFERRED_SIZE))
											.addGap(63)
											.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(scrollSD, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
												.addComponent(selectedGenres, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
												.addComponent(lblGenresSelected)
												.addComponent(removeGenrebtn)
												.addComponent(removeDirectorbtn)
												.addComponent(lblSelectedDirector))))
									.addGap(112))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(addDirectorbtn)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(newDirectorbtn))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(201)
							.addComponent(btnCreateNewMovie, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(101, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(titleField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblInsertTitle))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblInsertDuration)
						.addComponent(durationField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblInsertCountry)
						.addComponent(countryField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblSelectDateOf)
						.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSelectGenre)
						.addComponent(lblGenresSelected))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(selectedGenres, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
						.addComponent(genre, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(removeGenrebtn)
						.addComponent(addGenrebtn))
					.addGap(26)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSelectDirector)
						.addComponent(lblSelectedDirector))
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(scroll, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
						.addComponent(scrollSD, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(addDirectorbtn)
						.addComponent(newDirectorbtn)
						.addComponent(removeDirectorbtn))
					.addPreferredGap(ComponentPlacement.RELATED, 41, Short.MAX_VALUE)
					.addComponent(btnCreateNewMovie, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		getContentPane().setLayout(groupLayout);
	}

	public HashSet<String> getHashSet(ResultSet rs,int i) {
//		ListMultimap<Integer, String> multimap = ArrayListMultimap.create();
		HashSet<String> set = new HashSet<String>();
		
		try {
			while (rs.next()) {
				set.add(rs.getString(i));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return set;

	}
	
	public void fillList(DefaultListModel<String> model ,Collection<String> set) {
		
		for ( String genre: set ){
			  model.addElement(genre);
	     }
	}
		public void removeElementsFromList(DefaultListModel<String> model ,Collection<String> set) {
			for ( String genre: set ){
				  model.removeElement(genre);
		     }

  }
	
	ActionListener ButtonListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource()==addGenrebtn) {
				HashSet<String> selectedGenre= new HashSet<String>();
				  selectedGenre.addAll(genre.getSelectedValuesList());
				  fillList(selectedGenresModel,genre.getSelectedValuesList());
				    selectedGenreList.addAll(genre.getSelectedValuesList());
		            removeElementsFromList(genreModel,selectedGenre);
    		}
			if(e.getSource()==removeGenrebtn) {
				selectedGenreList.removeAll(selectedGenres.getSelectedValuesList());
				fillList(genreModel,selectedGenres.getSelectedValuesList());
				removeElementsFromList(selectedGenresModel,selectedGenres.getSelectedValuesList());
				}
			if(e.getSource()==addDirectorbtn) {
				HashSet<String> selectedDirector= new HashSet<String>();
				selectedDirector.addAll(director.getSelectedValuesList());
				fillList(selectedDirectorsModel,director.getSelectedValuesList());
			    selectedDirectorList.addAll(director.getSelectedValuesList());
	            removeElementsFromList(directorModel,selectedDirector);
	            
	            System.out.println(selectedDirectorList.toString());
				
			}
			if(e.getSource()==removeDirectorbtn) {
				selectedDirectorList.removeAll(selectedDirectors.getSelectedValuesList());
				fillList(directorModel,selectedDirectors.getSelectedValuesList());
				removeElementsFromList(selectedDirectorsModel,selectedDirectors.getSelectedValuesList());
				
			}
		}
		
	};
	
	ActionListener newDirector= new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			NewDirector();
			
		}
		
	};
	
	
	public void NewDirector() {
		nameField = new JTextField(14);
		dateOfBirthField = new JTextField(10);
		
		JPanel myPanel = new JPanel();
		myPanel.add(Box.createVerticalStrut(50));
		myPanel.add(new JLabel("NAME & SURNAME:"));
		myPanel.add(nameField);
		// a spacer
		myPanel.add(new JLabel("DATE OF BIRTH:(yyyy-mm-dd)"));
		myPanel.add(dateOfBirthField);

			
			
			int option = JOptionPane.showConfirmDialog(null,myPanel, "Please Enter the details of the new Director", JOptionPane.OK_CANCEL_OPTION);
			
			if (option == JOptionPane.CANCEL_OPTION){
		    }
			
			if(option == JOptionPane.OK_OPTION && movieCollection.checkDateFormat(dateOfBirthField.getText())==true) {
				if(movieCollection.insertNewDirector(nameField.getText(), Date.valueOf(dateOfBirthField.getText()))==true){	
					
				JOptionPane.showMessageDialog(null, "New Director added succesfully");
				directorList.add(nameField.getText());
				directorModel.addElement(nameField.getText());
				Director d= new Director(Date.valueOf(dateOfBirthField.getText()),nameField.getText()); // TODO  DIRECTOR ID
				directorNameId.put(d.getId(), d.getName());
				try {
					TimeUnit.SECONDS.sleep(3);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				  }
				else {JOptionPane.showMessageDialog(null, "Problem inserting 'new director'");
				  }
				}
			else {
			while (option == JOptionPane.OK_OPTION && movieCollection.checkDateFormat(dateOfBirthField.getText())==false){
				
					JOptionPane.showMessageDialog(null, "Wrong date format.\n This is the right date format: yyyy-mm-dd");
					option = JOptionPane.showConfirmDialog(null,myPanel, "Please Enter the details of the new Director", JOptionPane.OK_CANCEL_OPTION);
			    }
			}
			
					
			
		}
		
	ActionListener newMovieAL = new ActionListener() {
       ArrayList<Integer> director= new ArrayList<Integer>();
       ArrayList<String> genre= new ArrayList<String>();
       int movieId=-1;
		@Override
		public void actionPerformed(ActionEvent arg0) {
			java.util.Date d = dateChooser.getDate();
			java.sql.Date date = new java.sql.Date(d.getTime());
			
			for (String genres: selectedGenreList) {
				   genre.add(genres);
		  }
			
			for(String dir: selectedDirectorList ) {
				Set<Integer> keys = directorNameId.keySet();
			    for (Integer keyprint : keys) {
			        System.out.println("Key = " + keyprint);
			        Collection<String> values = directorNameId.get(keyprint);
			        for(String value : values){
			            System.out.println("Value= "+ value);
			            if(value.equals(dir)) {
			            	director.add(keyprint);
			            }
			        }
			    }
			}
			if((titleField.getText().isEmpty() ||durationField.getText().isEmpty()||countryField.getText().isEmpty() ==true) ||(director.isEmpty() || genre.isEmpty() ==true)){
				JOptionPane.showMessageDialog(null, "Fill all the fields");
			}
				else {
					
		    
			movieCollection.newMovie(titleField.getText() , date, durationField.getText(), countryField.getText());  //TODO id movie and date format
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			movieId= movieCollection.movieId(titleField.getText());
			
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			movieCollection.newMovie_genre_director(movieId, director, genre);
			
			JOptionPane.showMessageDialog(null, "Movie added succesfully!");

			}
		}
	};
		
	
	
		
	}

	

