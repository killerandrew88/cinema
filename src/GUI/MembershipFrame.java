package GUI;

import javax.swing.JPanel;

import DataStructure.Membership;
import Utilities.MembershipCollection;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JFrame;

public class MembershipFrame extends JFrame{
	
	private MembershipCollection memCol;
	private Membership newMembership;
	private JTextField textField;
	private JTextField textField_1;
	JButton btnNewButton;
	private String taxN;

	
	public MembershipFrame(String tax) {
		super();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setMinimumSize(new Dimension(400,350));
		pack();
		setLocationRelativeTo(null);
    	setMinimumSize(new Dimension(400,350));
		setVisible(true);
		
		setFont(new Font("Tahoma", Font.PLAIN, 14));
		setSize(new Dimension(310, 280));
		getContentPane().setLayout(null);
		
		memCol = new MembershipCollection();
		taxN = tax;
		
		
		JLabel lblTitle = new JLabel("Title: ");
		lblTitle.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTitle.setBounds(124, 105, 46, 14);
		getContentPane().add(lblTitle);
		
		JLabel lblDiscount = new JLabel("Discount:");
		lblDiscount.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDiscount.setBounds(98, 155, 95, 14);
		getContentPane().add(lblDiscount);
		
		textField = new JTextField();
		textField.setBounds(203, 104, 119, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(203, 154, 119, 20);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		
		btnNewButton = new JButton("Save");
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnNewButton.setBounds(120, 240, 138, 23);
		btnNewButton.addActionListener(new ActionListener(){
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String title= textField.getText();
			if (!textField_1.getText().matches("-?\\d+")){
                JOptionPane.showMessageDialog(null, "Discount must be a number");
                return;                    
            } 
			int discount = getDiscount();
			Date reg = new Date(System.currentTimeMillis());
			LocalDate acq_date = reg.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			
			newMembership = new Membership(acq_date,title,discount);
			boolean ok = memCol.saveMembershipAndCustomer(newMembership, taxN);
			if (ok){
				textField_1.setText("");
				textField.setText("");
			} else {
				JOptionPane.showMessageDialog(null, "Insertion of membership could not be completed!");
			}
			}
		});
		getContentPane().add(btnNewButton);
		
		JLabel lblMembershipForUser = new JLabel("MEMBERSHIP for user:");
		lblMembershipForUser.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblMembershipForUser.setBounds(32, 31, 139, 20);
		getContentPane().add(lblMembershipForUser);
		
		JLabel lblTaxno = new JLabel(taxN);
		lblTaxno.setBounds(212, 36, 46, 14);
		getContentPane().add(lblTaxno);
	}
	

	public void setActionListener(ActionListener listener) {
		btnNewButton.addActionListener(listener);
	}
//	
//	public String getTitle(){
//		return textField.getText();
//	}
//	
	public int getDiscount(){	
		int discount = Integer.parseInt(textField_1.getText());
		return discount;
	}
	
	public String textDiscount(){
		return textField_1.getText();
	}
	
	public Membership getMembership (){
		return newMembership;
	}
}
