
package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.awt.Dimension;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.*;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;

import com.jgoodies.forms.*;

import DataStructure.ActiveUser;
import DataStructure.Customer;
import Table.ReservationTableByProjModel;
import Table.ReservationTableEmailModel;
import Table.ReservationTableUserIdModel;
import Table.TableEmail;
import Table.TableProj;
import Table.ReservationTableByProjModel;

import Table.TableUserId;
import Table.projectionTables.IntelligentTable;
import Utilities.DBManager;
import Utilities.ReservationCollection;
import Utilities.UserCollection;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingUtilities;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

public class ManagementFrame extends JFrame implements ActionListener {

	private JPanel contentPane, seatPanel, container;
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem menuItem1, logout;
	private JTabbedPane MainTabbedPane;
	private JPanel Mainpanel;
	private JPanel reservationPanel;
	private JLabel lblProjection, LabelDatiUser;
	private SeatFrame seatFrame;
	private ReservationTableUserIdModel UserId_model;
	private ReservationTableByProjModel Proj_model;
	private ReservationTableEmailModel Email_model;
	private TableUserId tableUserId;
	private ReservationCollection resCol = new ReservationCollection();
	private ArrayList reservationList;
	private GroupLayout gl_Mainpanel, gl_Mainpanel_1;
	private MoviePanel movie_panel;
	private ProjPanel proj_panel; 
	private UserPanel user_panel;
	private JTextField textField;
	private ButtonGroup btnGroup;
	private ActiveUser activeU;
	private IntelligentTable table ;
	private TableProj tableProj;
	private TableEmail tableEmail;
	private JButton btnSearch;
	private JRadioButton rdbtnUserid, rdbtnProjectionId,rdbtnUserEmail;
	private JScrollPane scrollPane;

	private DisplayUserData dataPanel;
	private UserCollection userColl;
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//	
//					
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */

	public ManagementFrame(ActiveUser au) {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		
		activeU = au;
		proj_panel = new ProjPanel(activeU);
		movie_panel= new MoviePanel();
		if (activeU.isAdmin())
		user_panel= new UserPanel(activeU.getActiveAdmin());
		System.out.println("is admin: "+activeU.isAdmin());
		userColl = new UserCollection();
		
		setMinimumSize(new Dimension(1280, 720));
		setResizable(false);
		setSize(new Dimension(1280, 720));

		// check activeUser

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 970, 633);
		contentPane = new JPanel();
		contentPane.setPreferredSize(new Dimension(1280, 720));
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		container = new JPanel();
		container.setPreferredSize(new Dimension(1280, 720));
		contentPane.add(container);
		container.setLayout(new BorderLayout(0, 0));

		MainTabbedPane = new JTabbedPane(JTabbedPane.TOP);
		MainTabbedPane.setPreferredSize(new Dimension(1280, 720));
		MainTabbedPane.setMinimumSize(new Dimension(1280, 720));
		MainTabbedPane.setBackground(Color.gray);
		MainTabbedPane.setFont(new Font("Arial", Font.BOLD, 14));
		container.add(MainTabbedPane, BorderLayout.NORTH);

		btnGroup = new ButtonGroup();

		// Menu
		menuBar = new JMenuBar();
		menu = new JMenu("Menu");
		menuItem1 = new JMenuItem("exit");

		menu.add(menuItem1);
		menuBar.add(menu);

		menuItem1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event2) {
				System.exit(0);
			}
		});
		logout = new JMenuItem("Logout", KeyEvent.VK_T);
		logout.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				dispose();
				JFrame frame = new JFrame("Cinema Log in");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().add(new LoginPanel(frame));
				frame.setJMenuBar(MenuCreator.createMenuBar());
				frame.setMinimumSize(new Dimension(750,600));
				frame.pack();
				frame.setLocationRelativeTo(null);
            	frame.setMinimumSize(new Dimension(750,600));
				frame.setVisible(true);
			}
			
		});
		menu.add(logout);

		setJMenuBar(menuBar);
//		createJTable();
		table= new IntelligentTable(UserId_model);
	    scrollPane = new JScrollPane(table);
	    scrollPane.setBackground(Color.WHITE);
		createGuiByUser();


	}

	public void createCustomerInterface() {
		LabelDatiUser = new JLabel("DATI USER");
		LabelDatiUser.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		// reservation table
		Mainpanel = new JPanel();
		Mainpanel.setPreferredSize(new Dimension(1280, 630));
		Mainpanel.setMinimumSize(new Dimension(1280, 720));
		Mainpanel.setBackground(Color.pink);
		MainTabbedPane.addTab("RESERVATION MANAGER", null, Mainpanel, null);

		fillResTableUserId(activeU.getActiveCust().getTax_no()); // TODO: CHANGE WITH ACTIVE USER		
		
		
		JButton MakeAResButton = new JButton("MAKE A RESERVATION");
		MakeAResButton.addActionListener(ChangeTabPane);
		
		panel = new JPanel();
		panel.setBackground(Color.PINK);
		dataPanel = new DisplayUserData(activeU.getActiveCust());
		dataPanel.setBackground(Color.PINK);
		panel.setLayout(null);
		dataPanel.setBounds(0,0,209,350);
		panel.add(dataPanel);
		
		gl_Mainpanel = new GroupLayout(Mainpanel);
		gl_Mainpanel.setHorizontalGroup(
			gl_Mainpanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_Mainpanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_Mainpanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_Mainpanel.createSequentialGroup()
							.addGroup(gl_Mainpanel.createParallelGroup(Alignment.LEADING, false)
								.addGroup(gl_Mainpanel.createSequentialGroup()
									.addGap(45)
									.addComponent(LabelDatiUser, GroupLayout.PREFERRED_SIZE, 174, GroupLayout.PREFERRED_SIZE))
								.addComponent(panel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 1000, GroupLayout.PREFERRED_SIZE))
						.addGroup(Alignment.TRAILING, gl_Mainpanel.createSequentialGroup()
							.addComponent(MakeAResButton)
							.addGap(448))))
		);
		gl_Mainpanel.setVerticalGroup(
			gl_Mainpanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_Mainpanel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_Mainpanel.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_Mainpanel.createSequentialGroup()
							.addComponent(LabelDatiUser, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addComponent(MakeAResButton)
					.addContainerGap(57, Short.MAX_VALUE))
		);
		Mainpanel.setLayout(gl_Mainpanel);

		reservationPanel = new JPanel();
		reservationPanel.setPreferredSize(new Dimension(1280, 720));
		reservationPanel.setBackground(Color.yellow);
		MainTabbedPane.addTab("PROJECTION", null, proj_panel, null);
		reservationPanel.setLayout(null);

		lblProjection = new JLabel("PROJECTION");
		lblProjection.setBounds(27, 13, 92, 36);
		reservationPanel.add(lblProjection);

	}

	public void createAdminInterface() {

		Mainpanel = new JPanel();
		Mainpanel.setPreferredSize(new Dimension(1280, 630));
		Mainpanel.setMinimumSize(new Dimension(1280, 720));
		Mainpanel.setBackground(Color.pink);
		MainTabbedPane.addTab("RESERVATION MANAGER", null, Mainpanel, null);

		JLabel lblReservationTable = new JLabel("RESERVATION TABLE");
		lblReservationTable.setFont(new Font("Calibri", Font.BOLD, 20));

		JLabel lblSearchInput = new JLabel("Search Input:");
		lblSearchInput.setFont(new Font("Calibri", Font.BOLD, 20));

		textField = new JTextField();
		textField.setFont(new Font("Calibri", Font.PLAIN, 18));
		textField.setColumns(10);

		rdbtnUserid = new JRadioButton("UserID");
		rdbtnUserid.setSelected(true);
		rdbtnUserid.setFont(new Font("Calibri", Font.BOLD, 18));
		rdbtnUserid.setBackground(Color.PINK);

		rdbtnProjectionId = new JRadioButton("Projection ID");
		rdbtnProjectionId.setFont(new Font("Calibri", Font.BOLD, 18));
		rdbtnProjectionId.setBackground(Color.PINK);

		rdbtnUserEmail = new JRadioButton("User Email");
		rdbtnUserEmail.setFont(new Font("Calibri", Font.BOLD, 18));
		rdbtnUserEmail.setBackground(Color.PINK);

		btnGroup.add(rdbtnUserEmail);
		btnGroup.add(rdbtnProjectionId);
		btnGroup.add(rdbtnUserid);

		JButton btnDeleteTheSelected = new JButton("Delete the selected Reservation");
		btnDeleteTheSelected.setFont(new Font("Calibri", Font.BOLD, 16));

		JButton btnMakeANew = new JButton("Make a new Reservation");
		btnMakeANew.addActionListener(ChangeTabPane);
		btnMakeANew.setFont(new Font("Calibri", Font.BOLD, 16));

//		fillResTableUserId("");
		btnSearch = new JButton("Search");
		btnSearch.addActionListener(SearchbtnA);

		gl_Mainpanel_1 = new GroupLayout(Mainpanel);
		gl_Mainpanel_1.setHorizontalGroup(gl_Mainpanel_1.createParallelGroup(Alignment.TRAILING).addGroup(gl_Mainpanel_1
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_Mainpanel_1.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_Mainpanel_1.createSequentialGroup().addComponent(btnDeleteTheSelected).addGap(81)
								.addComponent(btnMakeANew).addGap(163))
						.addGroup(gl_Mainpanel_1.createSequentialGroup()
								.addGroup(gl_Mainpanel_1.createParallelGroup(Alignment.LEADING)
										.addComponent(rdbtnUserEmail).addComponent(rdbtnProjectionId)
										.addComponent(rdbtnUserid)
										.addGroup(gl_Mainpanel_1.createSequentialGroup().addGap(7)
												.addComponent(btnSearch).addGap(18).addComponent(textField,
														GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE)))
								.addPreferredGap(ComponentPlacement.RELATED, 66, Short.MAX_VALUE)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 832, GroupLayout.PREFERRED_SIZE)
								.addContainerGap())))
				.addGroup(gl_Mainpanel_1.createSequentialGroup().addGap(61).addComponent(lblSearchInput)
						.addPreferredGap(ComponentPlacement.RELATED, 587, Short.MAX_VALUE)
						.addComponent(lblReservationTable).addGap(343)));
		gl_Mainpanel_1.setVerticalGroup(gl_Mainpanel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_Mainpanel_1.createSequentialGroup().addContainerGap()
						.addGroup(gl_Mainpanel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblReservationTable).addComponent(lblSearchInput))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_Mainpanel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 583, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_Mainpanel_1.createSequentialGroup()
										.addGroup(gl_Mainpanel_1.createParallelGroup(Alignment.BASELINE)
												.addComponent(textField, GroupLayout.PREFERRED_SIZE, 33,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(btnSearch))
										.addGap(12).addComponent(rdbtnUserid).addGap(2).addComponent(rdbtnProjectionId)
										.addGap(2).addComponent(rdbtnUserEmail)))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_Mainpanel_1.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnDeleteTheSelected).addComponent(btnMakeANew))
						.addGap(501)));
		Mainpanel.setLayout(gl_Mainpanel_1);

		
		MainTabbedPane.addTab("PROJECTION MANAGER", null, proj_panel, null);

		
		MainTabbedPane.addTab("MOVIE MANAGER", null, movie_panel, null);
		
		
		MainTabbedPane.addTab("USER MANAGER", null, user_panel, null);

	}

	ActionListener SearchbtnA = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			if (rdbtnUserid.isSelected()) {
				fillResTableUserId(textField.getText().toUpperCase());
			}
			if (rdbtnProjectionId.isSelected()) {
				fillResTableProj(textField.getText());

			}
			else if(rdbtnUserEmail.isSelected()) {
				System.out.print(textField.getText());
				fillResTableEmail(textField.getText());
				
			}

		}

	};

	ActionListener ChangeTabPane = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			MainTabbedPane.setSelectedIndex(1);

		}

	};
	private JPanel panel;

	public void fillResTableUserId(String userId) {
		reservationList = resCol.reservationListUserId(resCol.reservationQueryUserId(userId));
		createTableUserId(reservationList);
		UserId_model = (ReservationTableUserIdModel) tableUserId.getModel();
		setModelJTable(UserId_model);
//		table=  new IntelligentTable(UserId_model);
//		table.repaint();
	}

	public void fillResTableProj(String projId) {
		reservationList = resCol.reservationListProjId(resCol.reservationQueryProjId(projId));
		createTableProj(reservationList);
		Proj_model = (ReservationTableByProjModel) tableProj.getModel();
		setModelJTable(Proj_model);

		
	}
	
	public void fillResTableEmail(String email) {
		reservationList = resCol.reservationListEmail(resCol.reservationQueryEmail(email));
		createTableEmail(reservationList); // TODO createTableEmail();
		Email_model = (ReservationTableEmailModel) tableEmail.getModel();
		Email_model.updateReservation(reservationList);
		setModelJTable(Email_model);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
	
	private void setModelJTable(TableModel tableModel) {
		table.setModel(tableModel);
		table.repaint();
		
	}

	private void createTableUserId(ArrayList reservationList2) {
		tableUserId = new TableUserId(new ReservationTableUserIdModel((reservationList2)));
		tableUserId.setFillsViewportHeight(true);
		tableUserId.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		

	}
	

	private void createTableEmail(ArrayList reservationList) {
		tableEmail = new TableEmail(new ReservationTableEmailModel((reservationList)));
		tableEmail.setFont(new Font("Calibri", Font.PLAIN, 20));
		tableEmail.setFillsViewportHeight(true);
		tableEmail.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		
	}

	private void createTableProj(ArrayList reservationList2) {
		System.out.println("create table reserv list 2");
		tableProj = new TableProj(new ReservationTableByProjModel((reservationList2)));
		tableProj.setFont(new Font("Calibri", Font.PLAIN, 20));
		tableProj.setFillsViewportHeight(true);
		tableProj.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

		
//		container.add(scrollPane, BorderLayout.CENTER);

	}
	
//	private void createJTable() {
//	
//		table=new JTable() { 
//			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
//	           Component component = super.prepareRenderer(renderer, row, column);
//	           int rendererWidth = component.getPreferredSize().width;
//	           TableColumn tableColumn = getColumnModel().getColumn(column);
//	           tableColumn.setPreferredWidth(Math.max(rendererWidth + getIntercellSpacing().width, tableColumn.getPreferredWidth()));
//	           return component;
//	        }
//	    };
//	    
//	    table.setRowHeight(25);
//		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
//		table.setFont(new Font("Calibri", Font.PLAIN, 20));
//		
//		scrollPane = new JScrollPane(table, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
//				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//		
//	}
	
	private void createGuiByUser() {
		//System.out.println(activeU.getActiveAdmin().getName());
		if (activeU.isAdmin()== true) {
			createAdminInterface();
		}
		else if (activeU.isAdmin()==false) {
			System.out.println(activeU.getActiveCust().getName()+" "+activeU.getActiveCust().getTax_no());
			createCustomerInterface();
		}
	}
}

