package GUI;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

import DataStructure.ActiveUser;
import DataStructure.Admin;
import DataStructure.CUser;
import DataStructure.Customer;
import DataStructure.Password;
import Exceptions.InvalidPasswordException;
import Utilities.UserCollection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginPanel extends JPanel {
	
	private JFrame sFrame;
	private JTextField textField;
	private JPasswordField passwordField;
	private RegisterFrame registerPane;
	private JPanel panel_1;
	private UserCollection userColl;
	
	private JButton btnLogIn,btnRegister;
	//frame to be opened after log in
	private ManagementFrame mgmFrame;
	
	/**
	 * Create the panel.
	 */
	public LoginPanel(JFrame frame) {
		sFrame = frame;
		sFrame.setSize(750, 600);
		
		setUpPanel();
	}
	
	public void setUpPanel() {
//		setLayout(new BorderLayout(0, 0));
		userColl = new UserCollection();
		setLayout(null);
		setMinimumSize(new Dimension(500, 400));
		panel_1 = new JPanel();
		panel_1.setLocation(97, 52);
		
		panel_1.setSize(new Dimension(413, 336));
		add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblEnterUsername = new JLabel("Email:");
		lblEnterUsername.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEnterUsername.setBounds(72, 88, 100, 14);
		panel_1.add(lblEnterUsername);
		
		JLabel lblEnterPassword = new JLabel("Password:");
		lblEnterPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEnterPassword.setBounds(72, 129, 73, 14);
		panel_1.add(lblEnterPassword);
		
		btnLogIn = new JButton("Log in");
		btnLogIn.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnLogIn.setBounds(167, 177, 177, 28);
		btnLogIn.addActionListener(new LoginButtonListener());
		panel_1.add(btnLogIn);
		
		JLabel lblNotRegistered = new JLabel("Not registered? ");
		lblNotRegistered.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNotRegistered.setBounds(34, 243, 118, 27);
		panel_1.add(lblNotRegistered);
		
		btnRegister = new JButton("Register");
		btnRegister.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnRegister.setBounds(167, 243, 177, 28);
		btnRegister.addActionListener(new RegisterButtonListener());
		panel_1.add(btnRegister);
		
		textField = new JTextField();
		textField.setBounds(167, 87, 177, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(167, 128, 177, 20);
		panel_1.add(passwordField);
		
		JLabel lblLoginToYour = new JLabel("Login to your cinema account");
		lblLoginToYour.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblLoginToYour.setBounds(114, 31, 230, 28);
		panel_1.add(lblLoginToYour);
	}

	/**
	 * Listener for login:
	 * checks input, if user is in db and password is correct, shows the main tabbed panel;
	 * if user is not, asks for registration; if user enters the wrong password alert.
	 * @author Georgiana
	 *
	 */
	
	private class LoginButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
			String email = textField.getText();
			String pass = new String(passwordField.getPassword());
			Password password=null;
			
			Admin ad;
			Customer cust;
			ActiveUser au = null;
			
			/*
			 * If user has inserted both email and password, check if they are valid.
			 * If the email is not valid, user is not registered in db(show message to register).
			 * If it is valid but the password is wrong, show to rewrite password.
			 */
			if (!email.equals("") && !pass.equals("")){
				try {
					password = new Password(pass);
					cust = userColl.searchCustomerByEmail(email);
					ad = userColl.searchAdminByEmail(email);
					
					//the given email belongs to a registered user
					if (cust != null || ad!= null){
						//must check for validity of password
						//For entered password is computed hashcode, the one in db is already hashcode
						CUser us = new CUser();
						if (cust != null){
							us = cust;
						} else if (ad != null){
							us = ad;
						} else {
							JOptionPane.showMessageDialog(null, "Serious problem: neither customer nor admin...");
							return;
						}
						Password check = new Password("");
						check.setPassHashcode(us.getPassword());
						boolean same = check.equals(password);
						
						if (same){
//							JOptionPane.showMessageDialog(null, "You have logged in!");
							if (cust!= null){
								au = new ActiveUser(cust);
							} else if (ad!= null){
								au = new ActiveUser(ad);
							}
							btnLogIn.setEnabled(false);
							//do something with au
							//load main frame
							mgmFrame = new ManagementFrame(au);
							mgmFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
							mgmFrame.setResizable(false);
							mgmFrame.pack();
							mgmFrame.setLocationRelativeTo(null);
							//the start frame must not be seen
							sFrame.setVisible(false);
							mgmFrame.setVisible(true);
							
						} else {
							JOptionPane.showMessageDialog(null, "Password is wrong."
									+ "\nPlease insert right password.");
							passwordField.setText("");
						}
					} else  {
						JOptionPane.showMessageDialog(null, "Your are not registered.\n"
								+ "Please register!");
						return;
					}
					
				} catch (InvalidPasswordException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else if (email.equals("")){
				JOptionPane.showMessageDialog(null, "Please insert email and password!");
			}else if(pass.equals("")){
				JOptionPane.showMessageDialog(null, "Please insert your password!");
			} 
			
			
		}
		
	}
	/**
	 * 
	 * @author Georgiana
	 *
	 */
	class RegisterButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			sFrame.setVisible(false);
			registerPane = new RegisterFrame(sFrame);
			
//			registerPane=new RegisterPanel(sFrame);
//			registerPane.setSize(new Dimension(750,650));
//			remove(panel_1);
//			add(registerPane);
//			repaint();
			
		}
		
	}

}
