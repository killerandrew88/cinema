package GUI;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

/**
 * 
 * @author Georgiana
 *
 */
public class MenuCreator {

	private static JMenuBar menuBar;
	private static JMenu menu;
	private static JMenuItem exitMenu, aboutMenu, logout;
	private static JFrame frame;
	
	public static void setLogoutMenu(JFrame fram){
		logout = new JMenuItem("Logout", KeyEvent.VK_T);
		logout.addActionListener(new MenuListener());
		menu.add(logout);
		frame = fram;
	}
	
	/**
	 * 
	 * @return
	 */
	public static JMenuBar createMenuBar() {
		menuBar = new JMenuBar();
		menuBar.setFont(new Font("Helvetica",16,Font.PLAIN));
		menu = new JMenu("Menu");
		
		exitMenu = new JMenuItem("Exit", KeyEvent.VK_T);
		aboutMenu = new JMenuItem("About", KeyEvent.VK_T);
		
		exitMenu.addActionListener(new MenuListener());
		aboutMenu.addActionListener(new MenuListener());
		
		
		menu.add(exitMenu);
		menu.add(aboutMenu);
		
		menuBar.add(menu);
		
		return menuBar;
	}

	/**
	 * 
	 * @author Georgiana
	 *
	 */
	private static class MenuListener implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			Object source = event.getSource();
			if (source == exitMenu) {
				System.exit(0);
			}

			// displays information about the developer of the program.
			// FINISH THIS!
			if (source == aboutMenu) {
				JOptionPane.showMessageDialog(null,
						"Developer: \n" + "ID Number:  \n"
								);
			}
			
			if (source == logout){
				frame.dispose();
				JFrame frame = new JFrame("Cinema Log in");
				
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.getContentPane().add(new LoginPanel(frame));
				frame.setJMenuBar(MenuCreator.createMenuBar());
				frame.setMinimumSize(new Dimension(750,600));
				frame.pack();
				frame.setLocationRelativeTo(null);
            	frame.setMinimumSize(new Dimension(750,600));
				frame.setVisible(true);
			}

		}

	}

}