package GUI;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.swing.ButtonGroup;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.event.ListDataListener;

public class Registration extends JPanel {
	
	private JTextField textFieldTax;
	private JTextField textFieldName;
	private JTextField textFieldEmail;
	private JTextField textFieldPhone;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;
	JRadioButton rdbtnF,rdbtnM;
	private ButtonGroup group;
	private DateChooser dateChoos;
	
	
	public Registration(){
		super();
		setSize(470,400);
		setLayout(null);
		
		setUpLabels();
		setUpInput();
	}
	
	public void setUpLabels(){
		JLabel lblTaxNumber = new JLabel("Tax number:");
		lblTaxNumber.setBounds(0, 0, 131, 46);
		lblTaxNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblTaxNumber);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(0, 46, 131, 44);
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblName);
		
		JLabel lblEmail = new JLabel("Email:");
		lblEmail.setBounds(0, 90, 131, 44);
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblEmail);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(0, 288, 131, 44);
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblPassword);
		
		JLabel lblConfirmPassword = new JLabel("Confirm password:");
		lblConfirmPassword.setBounds(0, 343, 131, 44);
		lblConfirmPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblConfirmPassword);
		
		JLabel lblPhoneNumber = new JLabel("Phone number:");
		lblPhoneNumber.setBounds(0, 134, 131, 44);
		lblPhoneNumber.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(lblPhoneNumber);
		
		JLabel lblGender = new JLabel("Gender:");
		lblGender.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblGender.setBounds(0, 200, 109, 14);
		add(lblGender);
		
		JLabel lblDateOfBirth = new JLabel("Date of birth");
		lblDateOfBirth.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDateOfBirth.setBounds(0, 250, 109, 14);
		add(lblDateOfBirth);
	}
	
	public void setUpInput(){
		textFieldTax = new JTextField();
		textFieldTax.setBounds(159, 15, 241, 20);
		this.add(textFieldTax);
		textFieldTax.setColumns(10);
		
		textFieldName = new JTextField();
		textFieldName.setBounds(159, 60, 241, 20);
		add(textFieldName);
		textFieldName.setColumns(10);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(159, 104, 241, 20);
		add(textFieldEmail);
		textFieldEmail.setColumns(10);
		
		textFieldPhone = new JTextField();
		textFieldPhone.setBounds(160, 148, 240, 20);
		add(textFieldPhone);
		textFieldPhone.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(159, 302, 241, 20);
		add(passwordField);
		
		passwordField_1 = new JPasswordField();
		passwordField_1.setBounds(159, 357, 240, 20);
		add(passwordField_1);
		
		rdbtnF = new JRadioButton("F");
		rdbtnF.setBounds(159, 196, 109, 23);
		add(rdbtnF);
		
		rdbtnM = new JRadioButton("M");
		rdbtnM.setBounds(291, 196, 109, 23);
		add(rdbtnM);
		
		group = new ButtonGroup();
		group.add(rdbtnF);
		group.add(rdbtnM);
		
		dateChoos = new DateChooser("",new Date(System.currentTimeMillis()));
		dateChoos.setBounds(150,250,250,30);
		add(dateChoos);
		repaint();
	}
	
	
	public String getInputTax(){
		return textFieldTax.getText();
	}
	
	public String getInputName(){
		return textFieldName.getText();
	}
	
	public String getInputEmail(){
		return textFieldEmail.getText();
	}
	
	public String getInputPhone(){
		return textFieldPhone.getText();
	}

	public LocalDate getInputDateBirth(){
		Date dateIn = dateChoos.getDate();
		LocalDate dateBirth = dateIn.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return dateBirth;
	}
	
	public String getPassword(){
		return new String(passwordField.getPassword());
	}
	
	public String getPasswordCheck(){
		return new String(passwordField_1.getPassword());
	}
	
	public void setPasswordField(String s){
		passwordField.setText(s);
	}
	public void setInputFields(String s){
		textFieldTax.setText(s);
		textFieldName.setText(s);
		textFieldEmail.setText(s);
		textFieldPhone.setText(s);
		passwordField.setText(s);
		passwordField_1.setText(s);
	}

	public void setActionListener(ActionListener listener){
		rdbtnF.addActionListener(listener);
		rdbtnM.addActionListener(listener);
	}
	
	
	
	private class DateChooser extends JPanel implements ItemListener {

	    private static final long serialVersionUID = 1L;
	    private JComboBox myDay;
	    private JComboBox myMonth;
	    private JComboBox myYear;
	    private Collection<ItemListener> myListeners;

	    public DateChooser(String name) {
	        this(name, new Date(System.currentTimeMillis()));
	    }
	    
	    public DateChooser(String name, Date date) {
	    
//	        super(new FlowLayout(FlowLayout.LEFT));
	    	setLayout(new FlowLayout(FlowLayout.LEFT));
	        setMinimumSize(new Dimension(200,20));
	        myListeners = new HashSet<ItemListener>();
	        
	        myDay = new JComboBox(new RangeModel(1, 31));
	        myMonth = new JComboBox(new RangeModel(1, 12));
	        myYear = new JComboBox(new RangeModel(1930, 2018));
	        
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        myDay.setSelectedItem(calendar.get(Calendar.DAY_OF_MONTH));
	        myMonth.setSelectedItem(calendar.get(Calendar.MONTH) + 1);
	        myYear.setSelectedItem(calendar.get(Calendar.YEAR));
	        
	        myDay.addItemListener(this);
	        myMonth.addItemListener(this);
	        myYear.addItemListener(this);
	        
	        add(new JLabel(name));
	        add(myDay);        
	        add(myMonth);
	        add(myYear);
	        
	        doLayout();
	    }
	    
	    public void setEnabled(boolean enabled) {
	        myDay.setEnabled(enabled);
	        myMonth.setEnabled(enabled);
	        myYear.setEnabled(enabled);
	    }
	    
	    public Date getDate() {
	        if (!myDay.isEnabled()) {
	            return null;
	        }
	        
	        Calendar calendar = Calendar.getInstance();
	        calendar.clear();
	        calendar.set(Calendar.DAY_OF_MONTH, (Integer) myDay.getSelectedItem());
	        calendar.set(Calendar.MONTH, (Integer) myMonth.getSelectedItem() - 1);
	        calendar.set(Calendar.YEAR, (Integer) myYear.getSelectedItem());
	        
	        return calendar.getTime();
	    }
	    
	    private class RangeModel implements ComboBoxModel {
	        
	        private int myMin;
	        private int myMax;
	        private Object mySelection;

	        public RangeModel(int min, int max) {
	            myMin = Math.min(min, max);
	            myMax = Math.max(min, max);
	            mySelection = myMin;
	        }
	        public Object getSelectedItem() {
	            return mySelection;
	        }
	        public void setSelectedItem(Object anItem) {
	            mySelection = anItem;
	        }
	        public int getSize() {
	            return myMax - myMin + 1;
	        }
	        public Object getElementAt(int index) {
	            return myMin + index;
	        }
	        public void addListDataListener(ListDataListener l) {
	        }
	        public void removeListDataListener(ListDataListener l) {
	        }
	    }
	    
	    public void addItemListener(ItemListener l) {
	        myListeners.add(l);
	    }

	    public void itemStateChanged(ItemEvent e) {
	        for (ItemListener listener : myListeners) {
	            listener.itemStateChanged(e);
	        }
	    }
	}
}