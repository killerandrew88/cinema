package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import Table.MovieTableTitleModel;
import Table.ReservationTableEmailModel;
import Table.ReservationTableUserIdModel;
import Table.TableEmail;
import Table.TableTitle;
import Table.projectionTables.IntelligentTable;
import Utilities.MovieCollection;

import javax.swing.JTable;

public class MoviePanel extends JPanel{
	private JTextField textField;
	private JTable table;
	private JRadioButton rdbtnTitle,rdbtnDateOfProduction;
	private JButton btnAddNewFilm,btnDelete,btnGetWholeList;
	private ButtonGroup buttonGroup;
	private ArrayList movieList;
	private MovieCollection movieCollection;
	private MovieTableTitleModel titleModel;
	private TableTitle tableTitle;
	private JScrollPane scrollPane;
	
	
	
	public MoviePanel() {
		super();
		setPreferredSize(new Dimension(1280, 720));
		setBackground(Color.MAGENTA);
		
		JLabel lblMovieTable = new JLabel("MOVIE TABLE");
		lblMovieTable.setFont( new Font("Arial", Font.BOLD, 18));
		
		movieCollection= new MovieCollection();
        table= new IntelligentTable(titleModel);
//        table.setSize()
        scrollPane = new JScrollPane(table);
		
		JButton btnSearch = new JButton("SEARCH");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event2) {
				if(rdbtnTitle.isSelected()) {
				fillMovieTableTitle(textField.getText().toUpperCase());
			}
				if(rdbtnDateOfProduction.isSelected()) {
					checkDateCreateTableModel(textField.getText());
				}
			}
		});
		
		textField = new JTextField();
		textField.setColumns(10);
		
		rdbtnTitle = new JRadioButton("TITLE");
		rdbtnTitle.setSelected(true);
		
		
		rdbtnDateOfProduction = new JRadioButton("DATE OF PRODUCTION");
		
		buttonGroup= new ButtonGroup();
		buttonGroup.add(rdbtnDateOfProduction);
		buttonGroup.add(rdbtnTitle);
		
		btnAddNewFilm = new JButton("ADD NEW FILM");
		btnAddNewFilm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event2) {
		       NewMovieFrame newMovie=new NewMovieFrame();
			}
		});
		
		btnDelete = new JButton("DELETE");
		btnDelete.addActionListener(removeAL);
		
		
	    btnGetWholeList = new JButton("GET WHOLE LIST OF MOVIES");
	    btnGetWholeList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event2) {
		       fillMovieTableAllMovie();
			}
		});
		
		
		table.setPreferredSize(new Dimension(900, 580));
		
		JButton btnCreateAProjection = new JButton("CREATE A PROJECTION");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(rdbtnTitle)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnSearch)
									.addGap(18)
									.addComponent(textField, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
								.addComponent(rdbtnDateOfProduction)
								.addComponent(btnCreateAProjection)
								.addComponent(btnDelete)
								.addComponent(btnAddNewFilm)
								.addComponent(btnGetWholeList))
							.addPreferredGap(ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 900, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(lblMovieTable)
							.addGap(404))))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(76)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSearch)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(rdbtnTitle)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(rdbtnDateOfProduction)
					.addGap(69)
					.addComponent(btnGetWholeList)
					.addGap(18)
					.addComponent(btnAddNewFilm)
					.addGap(18)
					.addComponent(btnDelete)
					.addGap(18)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnCreateAProjection)
					.addContainerGap(285, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(21)
					.addComponent(lblMovieTable, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
					.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 601, GroupLayout.PREFERRED_SIZE)
					.addGap(35))
		);
		setLayout(groupLayout);
	}
	
	
	
	
	public void fillMovieTableTitle(String title) {
		movieList = movieCollection.movieList(movieCollection.movieQueryTitle(title));
		tableTitle = new TableTitle(new MovieTableTitleModel(movieList));
		tableTitle.setFillsViewportHeight(true);
		titleModel = (MovieTableTitleModel) tableTitle.getModel();
		setModelJTable(titleModel);
		
	}
	
	public void fillMovieTableDateOfProd(String date) {
		movieList = movieCollection.movieList(movieCollection.movieQueryDateOfProd(Date.valueOf(date)));
		tableTitle = new TableTitle(new MovieTableTitleModel(movieList));
		tableTitle.setFillsViewportHeight(true);
		titleModel = (MovieTableTitleModel) tableTitle.getModel();
		setModelJTable(titleModel);
//		table= new IntelligentTable(titleModel);
//		table.repaint();
		
	}
	
	public void fillMovieTableAllMovie() {
		movieList = movieCollection.movieList(movieCollection.allMovieQuery());
		tableTitle = new TableTitle(new MovieTableTitleModel(movieList));
		tableTitle.setFillsViewportHeight(true);
		titleModel = (MovieTableTitleModel) tableTitle.getModel();
     	setModelJTable(titleModel);
//		table= new IntelligentTable(titleModel);
//		table.repaint();
	}
	
	public void checkDateCreateTableModel(String date) {
    	if(checkDateFormat(date)==true) {
    		fillMovieTableDateOfProd(date);
    		System.out.print("check date format");
    	}
    	else System.out.println("wrong date format");

	}
	
	
	
	public void setModelJTable(TableModel model) {
		table.setModel(model);
		table.repaint();
		
	}
	
	public boolean checkDateFormat(String value) {
	  	java.util.Date date = null;
	  	Boolean dateFormat=true;
	  	try {
	  	    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	  	    date = sdf.parse(value);
	  	    if (!value.equals(sdf.format(date))) {
	  	        System.out.println("Invalid Date Format");
	  	        return dateFormat=false;
	  	    }
	  	} catch (ParseException ex) {
	  	    ex.printStackTrace();
	  	}
	  	if (date == null) {
	  		return dateFormat=false;
	  	    // Invalid date format
	  	}
	  	else return dateFormat;
	  	}
	
	ActionListener removeAL= new ActionListener() {
        
		@Override
		public void actionPerformed(ActionEvent e) {
			if(table.getSelectedRow()!=-1) {
			
			int movie_id= (Integer) table.getValueAt(table.getSelectedRow(),0);
			System.out.println("selected Row" +table.getSelectedRow());
//			int movie_id= Integer.parseInt(s);
			movieCollection.deleteMovie(movie_id);
			
			titleModel = (MovieTableTitleModel) table.getModel();
			titleModel.removeRow(table.getSelectedRow());
			}
		}
		
	};
}
