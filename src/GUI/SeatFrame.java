package GUI;

import java.awt.Dimension;

import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Panel;
import javax.swing.GroupLayout.Alignment;

import DataStructure.Seat;
import DataStructure.seatsOfProjection;

import java.awt.FlowLayout;
import java.awt.Canvas;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridLayout;

public class SeatFrame extends JFrame {
	seatsOfProjection seats;
	int gridrow = 1;
	int gridcol = 1;
	Container contentPane;
	JFrame thisframe = this;
	
	public SeatFrame(int proj_id,String tax_no) {
	setSize(900,900);
	setVisible(true);
	setResizable(false);

	contentPane= this.getContentPane();
	
	JPanel container = new JPanel();
	container.setPreferredSize(new Dimension(900, 900));
	container.setLayout(new BorderLayout());
	
	
		//upper panel start
		JPanel upperPanel = new JPanel();
		upperPanel.setPreferredSize(new Dimension(800,150));
			
			Canvas canvas = new Canvas();
			canvas.setBackground(Color.DARK_GRAY);
			canvas.setFont(new Font("Arial", Font.PLAIN, 12));
			canvas.setForeground(Color.BLACK);
			canvas.setBounds(new Rectangle(0, 0, 450, 20));
			upperPanel.add(canvas);
		
		//upper panel end
			
		//central panel start
		JPanel centralPanel = new JPanel();
		centralPanel.setPreferredSize(new Dimension(800,740));
		centralPanel.setLayout(new BorderLayout());
			
			JPanel matrixTopPanel = new JPanel();
			matrixTopPanel.setPreferredSize(new Dimension(100,50));
			
			JPanel leftCentral = new JPanel();
			leftCentral.setPreferredSize(new Dimension(50,740));
			
			JPanel rightCentral = new JPanel();
			rightCentral.setPreferredSize(new Dimension(50,740));
			
			
			JPanel matrixPanel = new JPanel();
			matrixPanel.setPreferredSize(new Dimension(200,500));
				
				//matrix grid start
				seats = new seatsOfProjection(proj_id); //put projection id
				gridrow = seats.getRoom().getNumRow();
				gridcol = seats.getRoom().getNumCol();
			
				JPanel gridPanel = new JPanel();
				gridPanel.setPreferredSize(new Dimension(650,500));
				
				JLabel ticketCostLbl = new JLabel("Actual ticket cost = " + seats.getReservationCost());
				
				gridPanel.setLayout(new GridLayout(gridrow,gridcol));
				for (Seat s:seats.getSeats()) {
					gridPanel.add(s);
					s.addActionListener(new ActionListener() {

						@Override
						public void actionPerformed(ActionEvent e) {
							Seat s = (Seat)e.getSource();							
							if (!(s.getBackground()==Color.red) && (!s.isTaken())) {
								seats.getNewReservation().add(s);
								System.out.println("Seat added");
							}
							else if (!(s.getBackground()==Color.red) && (s.isTaken())) {
								seats.getNewReservation().remove(s);
								System.out.println("Seat removed");
							}
							ticketCostLbl.setText("Actual ticket cost = " + seats.getReservationCost());
						}
						
					});
				}
				matrixPanel.add(gridPanel);
				//matrix grid end
			
			JPanel matrixBottomPanel = new JPanel();
			matrixBottomPanel.setPreferredSize(new Dimension(100,100));
			//bottom panel items
				JButton reserveBtn = new JButton("Create Reservation");
				reserveBtn.setFont(new Font("Arial",Font.BOLD,22));
				reserveBtn.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						if (seats.getNewReservation().size()<=0) {
							JOptionPane.showMessageDialog(null, "You have to select at least one seat to perform the reservation");
						}else {
							seats.performReservation(tax_no);	
							JOptionPane.showMessageDialog(null, "Reservation performed successfully. Total price: "+seats.getReservationCost());
							thisframe.dispose();
						}
	
					}
					
				});
				
				matrixBottomPanel.add(ticketCostLbl);
				matrixBottomPanel.add(reserveBtn);
		
		
		centralPanel.add(matrixTopPanel,BorderLayout.PAGE_START);
		centralPanel.add(matrixBottomPanel,BorderLayout.PAGE_END);
		centralPanel.add(leftCentral,BorderLayout.LINE_START);
		centralPanel.add(rightCentral,BorderLayout.LINE_END);
		centralPanel.add(matrixPanel,BorderLayout.CENTER);
			
		
		//central panel end
		
		
		
		
		//add panels to container
		container.add(upperPanel,BorderLayout.PAGE_START);
		container.add(centralPanel , BorderLayout.CENTER);
	
	
	contentPane.add(container);
	
	
	
	
	
	
	
	
	
	}
	
	
	}
