package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Component;

import javax.print.attribute.AttributeSet;
import javax.swing.Box;
import com.toedter.calendar.JDayChooser;

import DataStructure.Movie;
import DataStructure.Room;
import DataStructure.RoomCollection;
import Utilities.DBManager;
import Utilities.MovieCollection;
import Utilities.TimeSlot;
import Utilities.TimeSlotGenerator;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.UIManager;
import java.awt.Choice;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.text.AbstractDocument;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.SwingConstants;
import javax.swing.JCheckBox;
import javax.swing.JButton;
import javax.swing.DropMode;
import java.awt.List;
import java.awt.Toolkit;

public class NewProjectionFrame extends JFrame {
	protected Container container;
	private JPanel containerPanel;
	protected Font  basicFont = new Font("Helvetica",Font.PLAIN,20);
	private JFormattedTextField priceTxt;
	
	private DBManager dbm;
	private MovieCollection movieCollection;
	private ArrayList <Movie> movieList;
	private JComboBox movieBox;
	private JLabel lblMovie;
	private JLabel lblDate;
	private JDateChooser dateChooser;
	private JLabel lblRoom;
	private Choice roomChoice;
	private JLabel lblTime;
	private Choice timeChoice;
	private JLabel lblPrice;
	private JLabel lblextra;
	private JCheckBox threedChk;
	private JButton resetBtn;
	private JButton confirmBtn;
	private JButton exitBtn;
	
	
	
	private Movie selectedMovie;
	private RoomCollection roomCollection;
	private java.sql.Date dateOfProj;
	private java.sql.Date lastDate;
	private TimeSlotGenerator tsg;
	private java.sql.Time selectedTime;
	private boolean check = false;
	private String adminTaxNo;
	private NewProjectionFrame thisFrame = this;

	
	public NewProjectionFrame(String adminTaxNo) {	
		//set up variables and utiltities
		this.adminTaxNo = adminTaxNo;
		dbm = new DBManager();
		movieCollection = new MovieCollection();
		movieList = movieCollection.movieList(movieCollection.allMovieQuery());
		selectedMovie = null;
		
		//set up graphics
		initGUI();
		

		
		//set up things in graphics
		this.setUpMovieBox();
		this.setupRooms();
		
		//set up action listeners
		this.setUpActionListeners();
		
		
		
		
	}
	
	public void reset() {
		System.out.println("RESET");
		selectedMovie = null;
		dateOfProj = null;
		this.movieBox.setEnabled(true);
		this.dateChooser.setEnabled(true);
		this.hideDate();
		this.hideRoomAndTime();
		this.timeChoice.removeAll();
		this.selectedTime = null;
		this.hidePriceAndExtras();
		this.priceTxt.setValue(Float.valueOf(0));
		this.threedChk.setSelected(false);
		this.disableConfirmation();
		
	}
	
	
	
	

	
	public void initGUI(){
		this.addWindowListener();
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);	
		this.setVisible(true);
		this.container = this.getContentPane();
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel northPanel = new JPanel();
		northPanel.setPreferredSize(new Dimension(50, 50));
		northPanel.setBackground(Color.YELLOW);
		northPanel.setSize(new Dimension(30, 40));
		getContentPane().add(northPanel, BorderLayout.NORTH);
		northPanel.setLayout(new BorderLayout(0, 0));
		
		JPanel labelPanel = new JPanel();
		northPanel.add(labelPanel, BorderLayout.CENTER);
		
		JLabel lblNewProjection = new JLabel("New Projection");
		lblNewProjection.setFont(new Font("Arial", Font.BOLD, 14));
		labelPanel.add(lblNewProjection);
		
		JPanel leftCentralPanel = new JPanel();
		leftCentralPanel.setToolTipText("Insert the price in format 3.50");
		leftCentralPanel.setPreferredSize(new Dimension(410, 10));
		getContentPane().add(leftCentralPanel, BorderLayout.CENTER);
		GridBagLayout gbl_leftCentralPanel = new GridBagLayout();
		gbl_leftCentralPanel.columnWidths = new int[]{154, 0};
		gbl_leftCentralPanel.rowHeights = new int[]{20, 19, 0, 0, 0, 34, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_leftCentralPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_leftCentralPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		leftCentralPanel.setLayout(gbl_leftCentralPanel);
		
		lblMovie = new JLabel("1. Select the movie");
		lblMovie.setFont(new Font("Arial", Font.PLAIN, 16));
		lblMovie.setPreferredSize(new Dimension(150, 14));
		GridBagConstraints gbc_lblMovie = new GridBagConstraints();
		gbc_lblMovie.insets = new Insets(0, 0, 5, 0);
		gbc_lblMovie.gridx = 0;
		gbc_lblMovie.gridy = 1;
		leftCentralPanel.add(lblMovie, gbc_lblMovie);
		
		Component horizontalStrut = Box.createHorizontalStrut(20);
		GridBagConstraints gbc_horizontalStrut = new GridBagConstraints();
		gbc_horizontalStrut.insets = new Insets(0, 0, 5, 0);
		gbc_horizontalStrut.gridx = 0;
		gbc_horizontalStrut.gridy = 2;
		leftCentralPanel.add(horizontalStrut, gbc_horizontalStrut);
		
		movieBox = new JComboBox();
		movieBox.setFont(new Font("Dialog", Font.BOLD, 14));
		movieBox.setPreferredSize(new Dimension(330, 25));
		GridBagConstraints gbc_movieBox = new GridBagConstraints();
		gbc_movieBox.anchor = GridBagConstraints.NORTH;
		gbc_movieBox.insets = new Insets(0, 0, 5, 0);
		gbc_movieBox.gridx = 0;
		gbc_movieBox.gridy = 3;
		leftCentralPanel.add(movieBox, gbc_movieBox);
		movieBox.setMinimumSize(new Dimension(50, 20));
		
		lblDate = new JLabel("2.Select the Date");
		lblDate.setVisible(false);
		lblDate.setFont(new Font("Arial", Font.PLAIN, 16));
		GridBagConstraints gbc_lblDate = new GridBagConstraints();
		gbc_lblDate.anchor = GridBagConstraints.NORTH;
		gbc_lblDate.insets = new Insets(0, 0, 5, 0);
		gbc_lblDate.gridx = 0;
		gbc_lblDate.gridy = 4;
		leftCentralPanel.add(lblDate, gbc_lblDate);
		
		dateChooser = new JDateChooser();
		dateChooser.setVisible(false);
		dateChooser.setBackground(Color.GRAY);
		dateChooser.getCalendarButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		dateChooser.getCalendarButton().setPreferredSize(new Dimension(30, 19));
		dateChooser.setPreferredSize(new Dimension(250, 30));
		GridBagConstraints gbc_dateChooser = new GridBagConstraints();
		gbc_dateChooser.insets = new Insets(0, 0, 5, 0);
		gbc_dateChooser.fill = GridBagConstraints.VERTICAL;
		gbc_dateChooser.gridx = 0;
		gbc_dateChooser.gridy = 5;
		leftCentralPanel.add(dateChooser, gbc_dateChooser);
		
		lblRoom = new JLabel("3. Select the room");
		lblRoom.setVisible(false);
		lblRoom.setFont(new Font("Arial", Font.PLAIN, 16));
		GridBagConstraints gbc_lblRoom = new GridBagConstraints();
		gbc_lblRoom.insets = new Insets(0, 0, 5, 0);
		gbc_lblRoom.gridx = 0;
		gbc_lblRoom.gridy = 6;
		leftCentralPanel.add(lblRoom, gbc_lblRoom);
		
		roomChoice = new Choice();
		roomChoice.setVisible(false);
		roomChoice.setPreferredSize(new Dimension(100, 25));
		GridBagConstraints gbc_roomChoice = new GridBagConstraints();
		gbc_roomChoice.insets = new Insets(0, 0, 5, 0);
		gbc_roomChoice.gridx = 0;
		gbc_roomChoice.gridy = 7;
		leftCentralPanel.add(roomChoice, gbc_roomChoice);
		
		lblTime = new JLabel("4.Select the right time slot");
		lblTime.setVisible(false);
		lblTime.setFont(new Font("Arial", Font.PLAIN, 16));
		GridBagConstraints gbc_lblTime = new GridBagConstraints();
		gbc_lblTime.insets = new Insets(0, 0, 5, 0);
		gbc_lblTime.gridx = 0;
		gbc_lblTime.gridy = 8;
		leftCentralPanel.add(lblTime, gbc_lblTime);
		
		timeChoice = new Choice();
		timeChoice.setVisible(false);
		timeChoice.setFont(new Font("Arial", Font.BOLD, 14));
		timeChoice.setForeground(Color.BLACK);
		timeChoice.setPreferredSize(new Dimension(200, 40));
		GridBagConstraints gbc_timeChoice = new GridBagConstraints();
		gbc_timeChoice.insets = new Insets(0, 0, 5, 0);
		gbc_timeChoice.gridx = 0;
		gbc_timeChoice.gridy = 9;
		leftCentralPanel.add(timeChoice, gbc_timeChoice);
		
		lblPrice = new JLabel("5. Set a price");
		lblPrice.setVisible(false);
		lblPrice.setFont(new Font("Arial", Font.PLAIN, 16));
		GridBagConstraints gbc_lblPrice = new GridBagConstraints();
		gbc_lblPrice.insets = new Insets(0, 0, 5, 0);
		gbc_lblPrice.gridx = 0;
		gbc_lblPrice.gridy = 10;
		leftCentralPanel.add(lblPrice, gbc_lblPrice);
		
		NumberFormat amountFormat = NumberFormat.getNumberInstance();
		priceTxt = new JFormattedTextField(amountFormat);
		priceTxt.setBackground(Color.WHITE);
		priceTxt.setVisible(false);
		priceTxt.setHorizontalAlignment(SwingConstants.CENTER);
		priceTxt.setSize(new Dimension(40, 30));
		priceTxt.setPreferredSize(new Dimension(40, 30));
		priceTxt.setToolTipText("Insert the price in format : 10.50\r\n");
		priceTxt.setText("00.00");
		priceTxt.setFont(new Font("Arial", Font.BOLD, 15));
		GridBagConstraints gbc_priceTxt = new GridBagConstraints();
		gbc_priceTxt.insets = new Insets(0, 0, 5, 0);
		gbc_priceTxt.gridx = 0;
		gbc_priceTxt.gridy = 11;
		leftCentralPanel.add(priceTxt, gbc_priceTxt);
		priceTxt.setColumns(10);
		
		lblextra = new JLabel("6.Extra Options");
		lblextra.setVisible(false);
		lblextra.setFont(new Font("Arial", Font.PLAIN, 16));
		GridBagConstraints gbc_lblextra = new GridBagConstraints();
		gbc_lblextra.insets = new Insets(0, 0, 5, 0);
		gbc_lblextra.gridx = 0;
		gbc_lblextra.gridy = 12;
		leftCentralPanel.add(lblextra, gbc_lblextra);
		
		threedChk = new JCheckBox("3D Movie");
		threedChk.setVisible(false);
		GridBagConstraints gbc_threedChk = new GridBagConstraints();
		gbc_threedChk.insets = new Insets(0, 0, 5, 0);
		gbc_threedChk.gridx = 0;
		gbc_threedChk.gridy = 13;
		leftCentralPanel.add(threedChk, gbc_threedChk);
		
		resetBtn = new JButton("Reset all data");
		resetBtn.setEnabled(false);
		resetBtn.setBackground(Color.ORANGE);
		resetBtn.setForeground(Color.BLACK);
		GridBagConstraints gbc_resetBtn = new GridBagConstraints();
		gbc_resetBtn.insets = new Insets(0, 0, 5, 0);
		gbc_resetBtn.gridx = 0;
		gbc_resetBtn.gridy = 16;
		leftCentralPanel.add(resetBtn, gbc_resetBtn);
		
		confirmBtn = new JButton("Confirm Projection");
		confirmBtn.setEnabled(false);
		confirmBtn.setBackground(new Color(152, 251, 152));
		GridBagConstraints gbc_confirmBtn = new GridBagConstraints();
		gbc_confirmBtn.insets = new Insets(0, 0, 5, 0);
		gbc_confirmBtn.gridx = 0;
		gbc_confirmBtn.gridy = 17;
		leftCentralPanel.add(confirmBtn, gbc_confirmBtn);
		
		exitBtn = new JButton("Exit");
		exitBtn.setForeground(Color.RED);
		GridBagConstraints gbc_exitBtn = new GridBagConstraints();
		gbc_exitBtn.gridx = 0;
		gbc_exitBtn.gridy = 20;
		leftCentralPanel.add(exitBtn, gbc_exitBtn);
		
		JPanel rightCentralPanel = new JPanel();
		rightCentralPanel.setPreferredSize(new Dimension(1, 10));
		getContentPane().add(rightCentralPanel, BorderLayout.EAST);
		this.setSize(420,680);
		
		//SET UP PANELS
		containerPanel = new JPanel();  //main container panel
		containerPanel.setLayout(new BorderLayout());
	    containerPanel.setPreferredSize(new Dimension(350,568));
	    containerPanel.setBackground(Color.red);
	    setLocationRelativeTo(null);
	}

	
	
	private void setUpActionListeners() {
		this.movieBox.addActionListener(movieBoxListener);
		this.resetBtn.addActionListener(resetListener);
		this.roomChoice.addItemListener(roomListener);
		this.dateChooser.addPropertyChangeListener(dateListener);
		this.timeChoice.addItemListener(timeListener);
		this.confirmBtn.addActionListener(confirmListener);
		this.exitBtn.addActionListener(exitListener);
	}
	
	
	private void setUpMovieBox() {
		for (Movie m : this.movieList) {
			this.movieBox.addItem(m.getTitle());
		}
	}
	
	private void setupRooms() {
		String query = "SELECT * FROM room";
		this.roomCollection = new RoomCollection();
		dbm.query(query);
		this.roomCollection.loadList(dbm.getRs());
		dbm.closeSets();
		for (Room r : this.roomCollection.getRoomList()) {
			 this.roomChoice.addItem(r.getRoomNum()+"");
		}
	}
	private void createTimeSlots(int roomNo) { //create time slots for 
		this.timeChoice.removeAll();
		String query = "select p.start_time, m.duration " + 
				"from projection p,room r , movie m " + 
				"where p.room_no = r.room_no and p.day = '"+this.dateOfProj+"' and " + 
				"p.movie_id = m.id and r.room_no = "+roomNo+ 
				" ORDER BY start_time ASC;";
		System.out.println(query);
		dbm.query(query);
		ArrayList<TimeSlot> reservedSlots = getReservedSlots(dbm.getRs());
		dbm.closeSets();
		LocalTime cinemaStartTime = LocalTime.of(16, 00);
		LocalTime cinemaCloseTime = LocalTime.of(23, 59);
		tsg = new TimeSlotGenerator(cinemaStartTime,cinemaCloseTime,reservedSlots,this.selectedMovie.getDuration(),10);
		System.out.println("tsg size: "+tsg.getSlots().size());
		for(TimeSlot slot : tsg.getSlots()) {
			System.out.println("slot start time " +slot.getStarTime().toString());
			this.timeChoice.add(slot.getStarTime() +"-->"+ slot.getStarTime().plusMinutes(selectedMovie.getDuration()));
		}		
	}
	@SuppressWarnings("deprecation")
	private ArrayList<TimeSlot> getReservedSlots(ResultSet rs){
		ArrayList<TimeSlot> slots = new ArrayList<TimeSlot>();
		if (rs!=null) {
			try {
				while (rs.next()){
					java.sql.Time time = rs.getTime(1);
					int duration;
					duration = rs.getInt(2);
					System.out.println(time.getHours() +"--"+ time.getMinutes());
					LocalTime start = LocalTime.of(time.getHours(),	 time.getMinutes());
					TimeSlot timeSlot = new TimeSlot(start,start.plusMinutes(duration));
					slots.add(timeSlot);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return slots;
	}
	
	private void showDate() {
		this.dateChooser.setMinSelectableDate(Date.valueOf(LocalDate.now()));
		this.dateChooser.setVisible(true);
		this.lblDate.setVisible(true);
	}
	
	
	private void hideDate() {
		this.dateChooser.setVisible(false);
		this.lblDate.setVisible(false);		
	}
	
	private void enableConfirmation() {
		this.check = false;
		this.confirmBtn.setEnabled(true);
	}
	private void disableConfirmation() {
		this.check = false;
		this.confirmBtn.setEnabled(false);
	}
	private void showRoomAndTime() {
		this.lblRoom.setVisible(true);
		this.lblTime.setVisible(true);
		this.timeChoice.setVisible(true);
		this.roomChoice.setVisible(true);
	}
	
	private void hideRoomAndTime() {
		this.lblRoom.setVisible(false);
		this.lblTime.setVisible(false);
		this.timeChoice.setVisible(false);
		this.roomChoice.setVisible(false);
	}
	private void showPriceAndExtras() {
		this.priceTxt.setVisible(true);
		this.lblPrice.setVisible(true);
		this.lblextra.setVisible(true);
		this.threedChk.setVisible(true);
	}
	private void hidePriceAndExtras() {
		this.priceTxt.setVisible(false);
		this.lblPrice.setVisible(false);
		this.lblextra.setVisible(false);
		this.threedChk.setVisible(false);
	}
	
	private void createNewProjection() {
		java.sql.Timestamp now = java.sql.Timestamp.valueOf(LocalDateTime.now());
		String threed = this.threedChk.isSelected() ? "yes" : "no";
		String query = "INSERT INTO Projection (movie_id, room_no, day, start_time, price, created_by, creation_time, threeD_option)" + 
				"VALUES" + 
				"("+this.selectedMovie.getId()+","+Integer.parseInt(this.roomChoice.getSelectedItem())+",'"+this.dateOfProj+"','"+this.selectedTime+"',"+this.getPrice()+",'"+this.adminTaxNo+"','"+now+"' ,'"+threed+"');";
		System.out.println(query);
		dbm.updateSql(query);
		dbm.closeSets();
	}
	
	
	ActionListener movieBoxListener = new ActionListener() {
		String movieTitle;
		@Override
		public void actionPerformed(ActionEvent e) {
			JComboBox cb = (JComboBox) e.getSource();
			movieTitle = cb.getSelectedItem().toString();
			cb.setEnabled(false);
			for (Movie m : movieList) {
				if (m.getTitle().equals(movieTitle)) {
					//set movie and show date
					selectedMovie = m;
					System.out.println(selectedMovie.getTitle());
					resetBtn.setEnabled(true);
					showDate();
				}
			}
		}
		
	};
	
	ActionListener resetListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			reset();
			showDate();
			reset();
			
		}
		
	};
	
	
	ItemListener roomListener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			Choice rc = (Choice) e.getSource();
			createTimeSlots(Integer.parseInt(rc.getSelectedItem()));
		}

		
		
	};
	
	ItemListener timeListener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			Choice tc = (Choice) e.getSource();
			String item = tc.getSelectedItem();
			String times[] = item.split("-->");
			String stime = times[0];
			LocalTime startTime = LocalTime.of(Integer.parseInt(stime.substring(0, 2)),Integer.parseInt( stime.substring(3, 5)));
			selectedTime = selectedTime.valueOf(startTime);
			showPriceAndExtras();
			enableConfirmation();
		}

		
		
	};
	
	
	PropertyChangeListener dateListener =  new PropertyChangeListener() {

		@Override
		public void propertyChange(PropertyChangeEvent e) {
			JDateChooser dc = (JDateChooser) e.getSource();			
			if (dc !=null) {
				java.util.Date d = dc.getDate();
				java.sql.Date temp = new java.sql.Date(d.getTime());
				if (!temp.equals(lastDate)) {
					dateOfProj = new java.sql.Date(d.getTime());
					lastDate = new java.sql.Date(d.getTime());
					dc.setEnabled(false);		
					showRoomAndTime();
				}else {
					dc.setEnabled(true);
				}	
			}
			

		}
		
	};
	
	ActionListener confirmListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			JButton btn = (JButton) e.getSource();
			if (isPriceOk() && isTimeOk()) {
				if (check) {
					//peform add operation
					btn.setEnabled(false);
					createNewProjection();
					dateChooser.removePropertyChangeListener(dateListener);
					thisFrame.dispose();
				}else {
					btn.setBackground(Color.GREEN);
					JOptionPane.showMessageDialog(null, "Check the data twice before confirming, this operation cannot be undone");
					check = true;					
				}
			}
			else {
				JOptionPane.showMessageDialog(null, "Price format not valid or wrong time slot selected.");
			}
		}
		
	};
	
	
	ActionListener exitListener = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			dateChooser.removePropertyChangeListener(dateListener);
			thisFrame.dispose();
			
		}
		
	};
	 	
	private void addWindowListener() {
		 this.addWindowListener(new WindowAdapter(){
             public void windowClosing(WindowEvent e){
                dateChooser.removePropertyChangeListener(dateListener);             }
         });
	}
	
	
	private boolean isPriceOk() {
		float price = Float.parseFloat(this.priceTxt.getValue().toString());
		if (price >0)
			return true;
		else
			return false;
	}
	private boolean isTimeOk() {
		if (this.timeChoice.getSelectedItem().equals("")) {
			return false;
		}else {
			return true;
		}
	}
	private float getPrice() {
		float price = Float.parseFloat(this.priceTxt.getValue().toString());
		return price;
	}


	

	
	
	
	
	
	
	
}
