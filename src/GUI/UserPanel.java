package GUI;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.table.AbstractTableModel;

import DataStructure.ActiveUser;
import DataStructure.Admin;
import DataStructure.CUser;
import DataStructure.Customer;
import UserTables.AdminTableModel;
import UserTables.AllUserTable;
import UserTables.AllUserTableModel;
import UserTables.CustomerTableModel;
import Utilities.UserCollection;

import javax.swing.JRadioButton;
import javax.swing.JScrollPane;

import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class UserPanel extends JPanel {

	private JTextField textField;
	private JButton btnShowCustomers, btnSearch, btnShowAdmins, btnShowAllUsers, btnSaveChanges, btnAddCustomer,
			btnAddAdministrator, btnDelete, btnMembership;
	private JRadioButton rdbtnEmail, rdbtnTax;
	private ButtonGroup group;
	private JPanel backGround;
	private JLabel title;
	ButtonListener listener;
	SearchListener listenerSearch;

	private AllUserTable userTable;
	private AllUserTableModel userModel;
	private AdminTableModel adminModel;
	private CustomerTableModel customerModel;

	private UserCollection collection;
	private ArrayList<CUser> allUsers;
	private ArrayList<Admin> allAdmins;
	private ArrayList<Customer> allCustomers;
	private Admin activeAdmin;

	public UserPanel(Admin activeAdmin) {
		super();
		setSize(new Dimension(1212, 714));
		setBackground(new Color(240, 230, 140));
		setLayout(null);
		this.activeAdmin = activeAdmin;

		collection = new UserCollection();
		listener = new ButtonListener();
		listenerSearch = new SearchListener();
		createInitialInterface();

	}

	private void createInitialInterface() {

		title = new JLabel("ALL USERS");
		title.setFont(new Font("Sylfaen", Font.BOLD, 20));
		title.setBounds(10, 13, 269, 37);
		add(title);

		btnShowAdmins = new JButton("Show Admins");
		btnShowAdmins.addActionListener(listener);
		btnShowAdmins.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnShowAdmins.setBounds(852, 13, 161, 30);
		add(btnShowAdmins);

		btnShowCustomers = new JButton("Show Customers");
		btnShowCustomers.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnShowCustomers.setBounds(583, 13, 161, 30);
		btnShowCustomers.addActionListener(listener);
		add(btnShowCustomers);

		btnShowAllUsers = new JButton("Show All Users");
		btnShowAllUsers.addActionListener(listener);
		btnShowAllUsers.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnShowAllUsers.setBounds(330, 13, 161, 30);
		add(btnShowAllUsers);

		btnSearch = new JButton("Search");
		btnSearch.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnSearch.setBounds(10, 60, 114, 23);
		btnSearch.addActionListener(listenerSearch);
		add(btnSearch);

		textField = new JTextField();
		textField.setBounds(134, 63, 145, 20);
		add(textField);
		textField.setColumns(10);

		rdbtnEmail = new JRadioButton("Email");
		rdbtnEmail.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnEmail.setSelected(true);
		rdbtnEmail.setBounds(10, 104, 99, 23);
		add(rdbtnEmail);

		rdbtnTax = new JRadioButton("Tax number");
		rdbtnTax.setFont(new Font("Tahoma", Font.PLAIN, 12));
		rdbtnTax.setBounds(10, 130, 99, 23);
		add(rdbtnTax);

		group = new ButtonGroup();
		group.add(rdbtnEmail);
		group.add(rdbtnTax);

		backGround = new JPanel();
		backGround.setBackground(Color.PINK);
		backGround.setBounds(330, 60, 873, 435);
		add(backGround);

		allUsers = collection.getUserList(collection.loadAllUsers());
		showUsers(backGround, allUsers);

		activateButtonSave();
		activateButtonAddAdmin();
		activateButtonAddCustomer();
		activateButtonDelete();
		activateButtonMembership();

		btnAddAdministrator.setVisible(false);
		btnAddCustomer.setVisible(false);
		btnSaveChanges.setVisible(false);
		btnDelete.setVisible(false);
		btnMembership.setVisible(false);
	}

	private void activateButtonSave() {
		btnSaveChanges = new JButton("SAVE CHANGES");
		btnSaveChanges.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnSaveChanges.setBounds(15, 465, 177, 30);
		btnSaveChanges.addActionListener(listener);
		add(btnSaveChanges);
	}

	private void activateButtonDelete() {
		btnDelete = new JButton("Delete selected row");
		btnDelete.addActionListener(listener);
		btnDelete.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnDelete.setBounds(10, 350, 182, 30);
		add(btnDelete);

	}

	private void activateButtonMembership() {

		btnMembership = new JButton("Add membership");
		btnMembership.setBounds(10, 309, 182, 30);
		btnMembership.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnMembership.addActionListener(listener);
		add(btnMembership);

	}

	private void activateButtonAddCustomer() {
		btnAddCustomer = new JButton("Add Customer");
		btnAddCustomer.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAddCustomer.setBounds(10, 227, 182, 30);
		btnAddCustomer.addActionListener(new RegisterListener());
		add(btnAddCustomer);

	}

	private void activateButtonAddAdmin() {
		btnAddAdministrator = new JButton("Add Administrator");
		btnAddAdministrator.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnAddAdministrator.setBounds(10, 268, 182, 30);
		btnAddAdministrator.addActionListener(new RegisterListener());
		add(btnAddAdministrator);
	}

	private void showUsers(JPanel panel, ArrayList<CUser> userList) {
		panel.removeAll();
		panel.setLayout(new GridLayout(1, 0));
		userModel = new AllUserTableModel(userList);
		userTable = new AllUserTable(userModel);
		userTable.setPreferredSize(new Dimension(800, 400));
		userTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane = new JScrollPane(userTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(810, 415));
		scrollPane.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel.add(scrollPane);
		panel.repaint();
		panel.revalidate();

	}

	private void showAdmins(JPanel panel, ArrayList<Admin> adminList) {
		panel.removeAll();
		panel.setLayout(new GridLayout(1, 0));
		adminModel = new AdminTableModel(adminList, activeAdmin);
		userTable = new AllUserTable(adminModel);
		userTable.setPreferredSize(new Dimension(800, 400));
		userTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane = new JScrollPane(userTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(810, 600));
		scrollPane.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel.add(scrollPane);
		panel.repaint();
		panel.revalidate();

	}

	private void showCustomers(JPanel panel, ArrayList<Customer> custList) {
		panel.removeAll();
		panel.setLayout(new GridLayout(1, 0));
		customerModel = new CustomerTableModel(custList);
		userTable = new AllUserTable(customerModel);
		userTable.setPreferredSize(new Dimension(800, 400));
		userTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
		JScrollPane scrollPane = new JScrollPane(userTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setPreferredSize(new Dimension(810, 600));
		scrollPane.setFont(new Font("Tahoma", Font.BOLD, 11));
		panel.add(scrollPane);
		panel.repaint();
		panel.revalidate();
	}

	private class SearchListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {

			AbstractTableModel tm = userTable.getTableModel();
			ArrayList<Admin> foundA = new ArrayList<>();
			ArrayList<Customer> foundC = new ArrayList<>();
			ArrayList<CUser> foundU = new ArrayList<>();
			String input = textField.getText();
			Admin a;
			Customer c;

			if (rdbtnEmail.isSelected()) {

				a = collection.searchAdminByEmail(input);
				c = collection.searchCustomerByEmail(input);

				if (a != null) {
					foundA.add(a);
					foundU.add((CUser) a);
				} else if (c != null) {
					foundC.add(c);
					foundU.add((CUser) c);
				} else {
					JOptionPane.showMessageDialog(null, "Cannot find the specified email\n" + "in user list");
				}

				if (tm instanceof AdminTableModel) {
					if (a != null) {
						showAdmins(backGround, foundA);
					} else {
						JOptionPane.showMessageDialog(null,
								"Cannot find the specified email" + "\nin administrator list!");
					}
				} else if (tm instanceof CustomerTableModel) {
					if (c != null) {
						showCustomers(backGround, foundC);
					} else {
						JOptionPane.showMessageDialog(null, "Cannot find the specified email" + "\nin customer list!");
					}

				}
				// In the case we are on the initial panel
				else {
					showUsers(backGround, foundU);
				}

			} else if (rdbtnTax.isSelected()) {
				a = collection.searchAdminByTax(input);
				c = collection.searchCustomerByTax(input);

				if (a != null) {
					foundA.add(a);
					foundU.add((CUser) a);
				} else if (c != null) {
					foundC.add(c);
					foundU.add((CUser) c);
				} else {
					JOptionPane.showMessageDialog(null, "Cannot find the specified tax number\n" + "in user list");
				}
				if (tm instanceof AdminTableModel) {

					if (a != null) {
						showAdmins(backGround, foundA);
					} else {
						JOptionPane.showMessageDialog(null,
								"Cannot find the specified tax number" + "\nin administrator list!");
					}
				} else if (tm instanceof CustomerTableModel) {
					System.out.println("I am searching a customer by tax.");

					if (c != null) {
						showCustomers(backGround, foundC);
					} else {
						JOptionPane.showMessageDialog(null,
								"Cannot find the specified tax number" + "\nin customer list!");
					}

				}
				// In the case we are on the initial panel
				else {
					showUsers(backGround, foundU);
				}

			}

		}

	}

	private class ButtonListener implements ActionListener {
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Object source = e.getSource();

			AbstractTableModel tm;
			CustomerTableModel ctm;
			AdminTableModel atm;

			if (source == btnShowAdmins) {
				title.setText("ADMINISTRATORS");
				btnAddAdministrator.setVisible(true);
				btnDelete.setVisible(true);
				btnAddCustomer.setVisible(false);
				btnMembership.setVisible(false);
				btnSaveChanges.setVisible(true);
				revalidate();

				allAdmins = collection.getAdminList(collection.loadAllAdmins());
				showAdmins(backGround, allAdmins);

			} else if (source == btnShowCustomers) {

				title.setText("CUSTOMERS");
				btnAddAdministrator.setVisible(false);
				btnDelete.setVisible(true);
				btnAddCustomer.setVisible(true);
				btnMembership.setVisible(true);
				btnSaveChanges.setVisible(true);
				revalidate();

				allCustomers = collection.getCustomerList(collection.loadAllCustomers());
				showCustomers(backGround, allCustomers);

			} else if (source == btnShowAllUsers) {

				title.setText("ALL USERS");
				btnAddAdministrator.setVisible(false);
				btnDelete.setVisible(false);
				btnAddCustomer.setVisible(false);
				btnAddCustomer.setVisible(false);
				btnSaveChanges.setVisible(false);
				revalidate();

				allUsers = collection.getUserList(collection.loadAllUsers());
				showUsers(backGround, allUsers);

			} else if (source == btnSaveChanges) {
				/*
				 * ATTENTION:difference between btnSaveChanges, which saves into
				 * db the list of users updated (changes in already existing
				 * users) and the method to add another user and save it!!!
				 */
				tm = userTable.getTableModel();
				if (tm instanceof CustomerTableModel) {
					ctm = (CustomerTableModel) tm;
					collection.updateAllUsers(ctm.getCustomerList());

				} else if (tm instanceof AdminTableModel) {
					atm = (AdminTableModel) tm;
					collection.updateAllUsers(atm.getAdminList());
				}

			} else if (source == btnDelete) {

				tm = userTable.getTableModel();
				int row = userTable.getSelectedRow();
				if (row != -1) {

					int c;
					if (tm instanceof CustomerTableModel) {
						ctm = (CustomerTableModel) tm;
						c = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete customer:" + "\n"
								+ ctm.getCustomerList().get(row).getTax_no() + "?" + "\nYour choice cannot be undone!");
						if (c == JOptionPane.YES_OPTION) {
							Customer cust = ctm.getCustomerList().get(row);
							ctm.removeRow(row);
							collection.deleteUser(new ActiveUser(cust));
						}

					} else if (tm instanceof AdminTableModel) {
						atm = (AdminTableModel) tm;
						c = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete admin:" + "\n"
								+ atm.getAdminList().get(row).getTax_no() + " ?");
						if (c == JOptionPane.YES_OPTION) {
							Admin admin = atm.getAdminList().get(row);
							atm.removeRow(row);
							collection.deleteUser(new ActiveUser(admin));
						}
					}
				}
			} else if (source == btnMembership){
				//just formal, I know it is customer model
				
				tm = userTable.getTableModel();
				int row = userTable.getSelectedRow();
				if (row != -1){
				if (tm instanceof CustomerTableModel) {
					ctm = (CustomerTableModel) tm;
					Customer cust = ctm.getCustomerList().get(row);
					MembershipFrame memFrame = new MembershipFrame(cust.getTax_no());
					
					
			}
		}

		}

	}
	}

	private class RegisterListener implements ActionListener{

		private Admin admin = new Admin();
		private Customer cust = new Customer();
		private RegisterFrame registrationF; 
		
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			Object source = e.getSource();
			
			 if(source == btnAddAdministrator){
				 
					registrationF = new RegisterFrame (admin);	
					
				}
			 else 
				 if (source == btnAddCustomer){
					 
					 registrationF = new RegisterFrame(cust);

				}
		}
		
	}
}
