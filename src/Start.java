import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import GUI.LoginPanel;
import GUI.MenuCreator;

public class Start {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFrame frame = new JFrame("Cinema Log in");
					
					frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame.getContentPane().add(new LoginPanel(frame));
					frame.setJMenuBar(MenuCreator.createMenuBar());
					frame.setMinimumSize(new Dimension(750,600));
					frame.pack();
					frame.setLocationRelativeTo(null);
                	frame.setMinimumSize(new Dimension(750,600));
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
