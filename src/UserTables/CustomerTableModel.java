package UserTables;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import DataStructure.Customer;

public class CustomerTableModel extends AbstractTableModel implements TableModelListener {
 
	private String[] columnNames = {"Tax no", "Name" , "Email" ,"Phone","Gender",
			"Date of birth","Id membership", "Registration date"};
	private ArrayList<Customer> custList;
	private Customer customer;
	
	public CustomerTableModel(ArrayList<Customer> customers) {
		custList = customers;
	}
	
	public ArrayList<Customer> getCustomerList(){
		return custList;
	}
	
	public void updateCustomerList(ArrayList<Customer> customers) {
		custList = customers;
		notifyChanges();
	}
	
	public void notifyChanges(){
		this.fireTableDataChanged();
	}

	public void removeRow(int row) {
		custList.remove(row);
		notifyChanges();
	}
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return custList.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnNames.length;
	}

	public String[] getColumnNames(){
		return this.columnNames;
	}
	
	 public String getColumnName(int col) {
         return columnNames[col];
     }
	 
	 //tax number and id of membership cannot be changed
	 //To change to id membership or grant a membership, 
	 //other data should be displayed.
	 public boolean isCellEditable(int row,int col){
			if (col==0 || col==6){
				return false;
			} else
			return true;
		}
	 
	 
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		customer = custList.get(rowIndex);
		
		switch (columnIndex){
		case 0: return customer.getTax_no();
		case 1: return customer.getName();
		case 2: return customer.getEmail();
		case 3: return customer.getPhone_no();
		case 4: return customer.getGender();
		case 5: return customer.getDate_birth();
		case 6: return customer.getId_mem();
		case 7: return customer.getReg_date();
		}
		
		return null;
	}

	/**
	 * Allows to save changes in the table
	 */
	public void setValueAt(Object value, int row, int col){
		
		Customer changed = custList.get(row);
		
		switch(col){
		case 1 :{ 
			changed.setName(value.toString());
			break;
		}
		case 2 : {
			changed.setEmail(value.toString());
			break;
		}
		case 3 : {
			changed.setPhone_no(value.toString());
			break;
		}
		case 4 : {
			changed.setGender(value.toString().charAt(0));
			break;
		}
		case 5 : {
			LocalDate date = LocalDate.parse(value.toString());
			changed.setDate_birth(date);
			break;
		}
		case 7:{
			LocalDate date = LocalDate.parse(value.toString());
			changed.setReg_date(date);
		}
		}
		System.out.println(changed);
		updateCustomerList(custList);
		fireTableCellUpdated(row,col);
		
	}

	@Override
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
				int row = e.getFirstRow();
				int col = e.getColumn();
				TableModel model = (TableModel) e.getSource();
				Object data = model.getValueAt(row,col);
				System.out.println(data.toString());
				this.setValueAt(data,row,col);
	}
}
