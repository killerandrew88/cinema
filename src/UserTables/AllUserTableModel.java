package UserTables;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import DataStructure.CUser;

public class AllUserTableModel extends AbstractTableModel implements TableModelListener{

	private String[] columnNames = {"Tax no", "Name" , "Email" ,"Phone","Gender","Type","Date of birth"};
	private ArrayList<CUser> userList;
	private CUser user;
	
	public AllUserTableModel(ArrayList<CUser> users){
		userList = users;
	}
	
	public String[] getColumnNames(){
		return this.columnNames;
	}
	
	 public String getColumnName(int col) {
         return columnNames[col];
     }
	 
	public void removeRow(int row){
		userList.remove(row);
		notifyChanges();
	}
	 
	public void updateUserList(ArrayList<CUser> users){
		this.userList = users;
		notifyChanges();
	}
	
	public void notifyChanges(){
		this.fireTableDataChanged();
	}
	
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return userList.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columnNames.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		user = userList.get(rowIndex);
		switch (columnIndex){
		
		case 0 : return user.getTax_no();
		case 1 : return user.getName();		
		case 2 : return user.getEmail();		
		case 3 : return user.getPhone_no();		
		case 4 : return user.getGender();
		case 5 : return user.getType();
		case 6 : return user.getDate_birth();
		}
		return null;
	}

	/*In this table user data cannot be changed
	Admin must access the "All admins" or "All customers" tables
	to modify user data */
	public boolean isCellEditable(int row,int col){
//		if (col>0){
//			return true;
//		} else
		return false;
	}
	
	 /*
     * Don't need to implement this method unless your table's
     * data can change.
     */ 
	//TODO : finish, and save changes in db
    public void setValueAt(Object value, int row, int col) {
    	CUser changed = userList.get(row);
    	
    	switch (col){
		
		case 1 :{ 
			changed.setName(value.toString());
			break;
		}
		case 2 : {
			changed.setEmail(value.toString());
			break;
		}
		case 3 : {
			changed.setPhone_no(value.toString());
			break;
		}
		case 4 : {
			changed.setGender(value.toString().charAt(0));
			break;
		}
		case 5 : {
			changed.setType(value.toString().charAt(0));
			break;
		}
		case 6 : {
			LocalDate date = LocalDate.parse(value.toString());
			changed.setDate_birth(date);
			}
		}
    	System.out.println(changed);
    	updateUserList(userList);
    	fireTableCellUpdated(row, col);
    	
    }
	
	/*
	 * TableModelListener implementation
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	@Override
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub
		int row = e.getFirstRow();
		int col = e.getColumn();
		TableModel model = (TableModel) e.getSource();
		Object data = model.getValueAt(row,col);
		System.out.println(data.toString());
		this.setValueAt(data,row,col);
		
	}
	
	/*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     */
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

}
