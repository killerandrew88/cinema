package UserTables;


import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import DataStructure.CUser;

public class AllUserTable extends JTable{

	private AbstractTableModel tableModel;
	
	public <TM extends AbstractTableModel> AllUserTable(TM tm){
		super(tm);
		tableModel = tm;
		this.setColumnSelectionAllowed(false);
		this.setRowSelectionAllowed(true);
		this.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		this.setCellSelectionEnabled(true);
		this.setPreferredScrollableViewportSize(new Dimension(800, 400));
        this.setFillsViewportHeight(true);
        this.setFont(new Font("Tahoma",Font.PLAIN,15));
        this.setRowHeight(20);
        this.getColumnModel().getColumn(getColumnModel().getColumnIndex("Gender")).setPreferredWidth(30);
//        this.getColumnModel().getColumn(5).setPreferredWidth(30);
        
	}
	
	public AbstractTableModel getTableModel(){
		return tableModel;
	}
}
