package UserTables;

import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

import DataStructure.Admin;

public class AdminTableModel extends AbstractTableModel implements TableModelListener {

	private String[] columnNames = {"Tax no", "Name" , "Email" ,"Phone","Gender","Date of birth","Permission"};
	private ArrayList<Admin> adminList;
	private Admin admin,activeAdmin;
	
	public AdminTableModel(ArrayList<Admin> admins,Admin activeA){
		adminList = admins;
		activeAdmin = activeA;
	}
	
	public void updateAdminList(ArrayList<Admin> admins){
		adminList = admins;
		notifyChanges();
	}
	
	public ArrayList<Admin> getAdminList(){
		return adminList;
	}
	
	public void notifyChanges(){
		this.fireTableDataChanged();
	}
	
	@Override
	public int getRowCount() {
		return adminList.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}
	
	public String[] getColumnNames(){
		return this.columnNames;
	}
	
	 public String getColumnName(int col) {
         return columnNames[col];
     }

	 public boolean isCellEditable(int row,int col){
		boolean editable = false;
		
		if (activeAdmin.getPermission()==2){
			 if (col>0){
			 editable = true;
			 }
		} else if (col>0 && col<6)
			editable = true;
		 
		return editable;
		}
	 
	public void removeRow(int row){
		adminList.remove(row);
		notifyChanges();
	}
	 
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		admin = adminList.get(rowIndex);
		
		switch(columnIndex){
		case 0: return admin.getTax_no();
		case 1: return admin.getName();
		case 2: return admin.getEmail();
		case 3: return admin.getPhone_no();
		case 4: return admin.getGender();
		case 5: return admin.getDate_birth();
		case 6: return admin.getPermission();
		}
		
		return null;
	}

	/*
     * Don't need to implement this method unless your table's
     * data can change.
     */ 
	//TODO : finish, and save changes in db
    public void setValueAt(Object value, int row, int col) {
    	Admin changed = adminList.get(row);
    	
    	switch (col){
		
		case 1 :{ 
			changed.setName(value.toString());
			break;
		}
		case 2 : {
			changed.setEmail(value.toString());
			break;
		}
		case 3 : {
			changed.setPhone_no(value.toString());
			break;
		}
		case 4 : {
			changed.setGender(value.toString().charAt(0));
			break;
		}
		case 5 : {
			LocalDate date = LocalDate.parse(value.toString());
			changed.setDate_birth(date);
			break;
		}
		case 6 : {
			changed.setPermission(Integer.parseInt(value.toString()));
			}
		}
    	System.out.println(changed);
    	updateAdminList(adminList);
    	fireTableCellUpdated(row, col);
    	
    }
	/*
	 * TableModelListener implementation
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	@Override
	public void tableChanged(TableModelEvent e) {
		// TODO Auto-generated method stub
		int row = e.getFirstRow();
		int col = e.getColumn();
		TableModel model = (TableModel) e.getSource();
		Object data = model.getValueAt(row,col);
		System.out.println(data.toString());
		this.setValueAt(data,row,col);
		
	}
}
