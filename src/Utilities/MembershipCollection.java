package Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import DataStructure.Membership;

public class MembershipCollection {
	private DBManager dbm;
	private Membership membership;
//	private int count ;
	
	public MembershipCollection(){
		dbm = new DBManager();
	}
	
	
	
	public boolean saveMembershipAndCustomer(Membership mem,String tax){
		
		boolean done = saveMembership(mem);
	
		done = updateMembershipInUser(tax,mem.getId());
		
		return done;
		
	}
	
	public boolean saveMembership(Membership membership){
		
		boolean done;
		membership.setId(countMembership()+1);
		
		String update = "INSERT INTO Membership ( id, acq_date, title, discount) " +
				 "VALUES (" + membership.getId() + ",'" + membership.getAcq_date().toString() + "','"
				 		+ membership.getTitle() + "','" +membership.getDiscount() +"');";
		done = dbm.updateSql(update);
		dbm.closeSets();
		System.out.println(membership);
		return done;
		
		
	}
	
	public boolean updateMembershipInUser(String tax,int memID){
		boolean done;
		
		String update = "UPDATE customer SET id_mem = " + memID +
				" WHERE UPPER(tax_no) = '" + tax.toUpperCase() +"';";
		
		done = dbm.updateSql(update);
		dbm.closeSets();
		
		return done;
	}
	
	
	public int countMembership(){
		int count=0;
		String query = "SELECT max(id) as count from membership;";
		dbm.query(query);
		ResultSet rs = dbm.getRs();
		if (rs != null)
		try {
			while(rs.next())
			    count=rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		dbm.closeSets();
		return count;
		
	}
	
}
