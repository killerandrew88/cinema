package Utilities;

import java.time.LocalTime;
import java.util.ArrayList;

public class TimeSlotGenerator {
	ArrayList <TimeSlot> slots;
	
	public ArrayList<TimeSlot> getSlots() {
		return slots;
	}

	public void setSlots(ArrayList<TimeSlot> slots) {
		this.slots = slots;
	}

	//reservedSlots must be an ordered array of time slots, from early to late. (offset_min = minimum time of offset beetween two projections, if one end at 7.50 and offset is 5 min, the other will have to start at 7.55)
	public TimeSlotGenerator(LocalTime startTime,LocalTime endTime,ArrayList<TimeSlot> reservedSlots,int requiredtime_min,int offset_min) {
		LocalTime lastFreeSlot = startTime;
		this.slots = new ArrayList<TimeSlot>();
		boolean freespace = true;
		ArrayList<TimeSlot> possibleSlots = generateAllPossibleSlots(startTime,endTime.minusMinutes(requiredtime_min),15);
		ArrayList<TimeSlot> resultSlots = (ArrayList<TimeSlot>) possibleSlots.clone();
		if (reservedSlots.size()==0) {
			//no reserved slots case
			this.slots = possibleSlots;
		}
		else {
			int i = 0;
			for (TimeSlot posSlot : possibleSlots) {			
				for (TimeSlot resSlot : reservedSlots) {
					if (posSlot.getStarTime().toSecondOfDay() == resSlot.getStarTime().toSecondOfDay()) {
						System.out.println("Removed time equal: "+ resultSlots.get(i).getStarTime() +" Possible Slot:"+posSlot.getStarTime());
						resultSlots.remove(i);
						i = i-1;
					}else if(posSlot.getStarTime().isBefore(resSlot.getStarTime()) && posSlot.getStarTime().plusMinutes(requiredtime_min).isAfter(resSlot.getStarTime())){
						System.out.println("Removed time before: "+ resultSlots.get(i).getStarTime() +" Possible Slot:"+posSlot.getStarTime());
						resultSlots.remove(i);
						i = i-1;
					}
					else if(posSlot.getStarTime().isAfter(resSlot.getStarTime()) && posSlot.getStarTime().isBefore(resSlot.getEndTime())) {
						System.out.println("Removed time after: "+ resultSlots.get(i).getStarTime() +" Possible Slot:"+posSlot.getStarTime());
						resultSlots.remove(i);
						i = i-1;
					}
				}
				i = i+1;
			}
			this.slots = resultSlots;
		}
		
	}
	public ArrayList<TimeSlot> generateAllPossibleSlots(LocalTime start,LocalTime end,int interval) {
		ArrayList<TimeSlot> timeSlotBuffer = new ArrayList<TimeSlot>();
		LocalTime finalEnd = end.minusMinutes(interval);
		boolean freeSpace = true;
		LocalTime actualTime = start;
		while(freeSpace) {
			if (actualTime.plusMinutes(interval).isAfter(finalEnd)) {
				freeSpace = false;
			}
			else {
				TimeSlot slot = new TimeSlot(actualTime,actualTime.plusMinutes(interval));
				timeSlotBuffer.add(slot);
				actualTime = actualTime.plusMinutes(interval);
			}	
		}
		
		
		return timeSlotBuffer;
	}
	
	
	
	
	/*public TimeSlotGenerator(LocalTime startTime,LocalTime endTime,ArrayList<TimeSlot> reservedSlots,int requiredtime_min,int offset_min) {
		LocalTime lastFreeSlot = startTime;
		this.slots = new ArrayList<TimeSlot>();
		boolean freespace = true;
		if (reservedSlots.size()==0) {
			while(freespace==true) {
				System.out.println("no reserved slots");
				if (lastFreeSlot.plusMinutes(requiredtime_min).isBefore(endTime)) {
					System.out.println("good slot found,  Last free slot : "+ lastFreeSlot);
					TimeSlot goodSlot = new TimeSlot(lastFreeSlot,lastFreeSlot.plusMinutes(requiredtime_min));
					slots.add(goodSlot);
					lastFreeSlot = goodSlot.getEndTime().plusMinutes(offset_min);
				}else {
					System.out.println("Not before end end time");
					freespace = false;
				}	
			}
		}
		else {
			for (TimeSlot slot : reservedSlots) {
				System.out.println("Reserved slot data : "+ slot.getStarTime() +" --> "+slot.getEndTime());
				LocalTime check = lastFreeSlot.plusMinutes(requiredtime_min);
				if (check.isAfter(slot.getStarTime().plusMinutes(offset_min)) && check.isBefore(slot.getEndTime().plusMinutes(offset_min))) {
					lastFreeSlot = slot.getEndTime().plusMinutes(offset_min);
				}
				else {
					TimeSlot goodSlot = new TimeSlot(lastFreeSlot,check);
					slots.add(goodSlot);
					lastFreeSlot = check;
				}
			}
		}
		
	}*/
	
	

}
