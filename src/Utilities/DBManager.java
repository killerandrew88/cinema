package Utilities;

import java.sql.*;

public class DBManager {

	private String DbUsername = "xwkfzive"; // TODO make it so that you can pass in

	private String DbPassword = "Fww8ZsAJL_PJSo47q60snC7TlU7JBTM9";
	
	private String DbName = "xwkfzive";
	
	private String driverClassName = "org.postgresql.Driver";
	
	private String url = "jdbc:postgresql://52.18.106.191:5432/"+ DbName;
	
	
	private Connection con;
	private Statement stm;
	private ResultSet rs;
	


	public DBManager() {
 
        try {
            // load, and link the jdbc driver class
            Class.forName(driverClassName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	
	 public Connection getCon() {
		return con;
	}

	public void setCon(Connection con) {
		this.con = con;
	}

	private Connection createCon() throws SQLException {
		 
	        con = DriverManager.getConnection(url, DbUsername,DbPassword);
	        // url,DbUsername, DbPassword);
	        return con;
	 }
	 
	 //USE THIS TO OBTAIN QUERY RESULT , CHECK ALWAYS IF RESULT ! NULL
	 public ResultSet getRs() {
		return rs;
	}

	public void setRs(ResultSet rs) {
		this.rs = rs;
	}

	private ResultSet queryFunction(String strSql) { //execute query and return result set
	        
		try {
	        	this.stm = this.createCon().createStatement();
	            ResultSet resultSet = this.stm.executeQuery(strSql);
	            return resultSet;
	        } catch (java.sql.SQLException ex) {
	           //handle exception here
	        	ex.printStackTrace();
	            return null;
	        }
	 }
	
	 public void query(String strSql) {
		 this.setRs(this.queryFunction(strSql)); //assign value to result set variable in the object DBMananger
	 }
	 
	 public boolean updateSql(String strSQL) {
         try{     
        	this.stm = this.createCon().createStatement();
            this.stm.executeUpdate(strSQL);
            this.stm.close();
            this.con.close();
           return true;
        } catch (java.sql.SQLException ex) {
            ex.printStackTrace();      	
            return false;
        }
	 }
	 
	 
	 //use this to close all connection after operations are done on result set.
	 public void closeSets() {
		 try {
			if (this.stm != null) {
				this.stm.close();	
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ERROR :  Statement closing problem ");
		}
		 try {
			if (this.rs != null) {
				this.rs.close();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("ERROR : Result set closing problem");
		}
		 try {
			 if(this.con !=null) {
				 this.con.close();
			 }
		 }
		 catch(SQLException e) {
			 e.printStackTrace();
			 System.out.println("ERROR :  Connection closing problem");
		 }
	 }


	
	
	
	
	
	/*ResultSet rs = // Load result set ...
	ResultSetMetaData rsmd = rs.getMetaData();
	int numColumns = rsmd.getColumnCount();
	List rows = new LinkedList();
	while (rs.next()) {
	  List columns = new LinkedList();
	  for (int i=1; i<=numColumns; i++) {
	    columns.add(rs.getString(i));
	  }
	  rows.add(columns);
	} */
	
}
