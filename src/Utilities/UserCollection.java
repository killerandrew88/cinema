package Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import DataStructure.ActiveUser;
import DataStructure.Admin;
import DataStructure.CUser;
import DataStructure.Customer;
import DataStructure.Password;
import Exceptions.InvalidPasswordException;

public class UserCollection {

	private DBManager dbm;
	private CUser user;
	private ArrayList<CUser> userList;
	private Admin admin;
	private Customer customer;
	private ArrayList<Admin> adminList;
	private ArrayList<Customer> customerList;
	
//	public static void main(String [] args) throws InvalidPasswordException{
//		
//		UserCollection coll = new UserCollection();
//		Password password;
//		
//		ArrayList<CUser> users = coll.getUserList(coll.loadAllUsers());
//		for (int i = 0;i<5; i++){
//			System.out.println(users.get(i));
//			String passInDB = new String(users.get(i).getPassword());
//			System.out.println(passInDB);
////			password = new Password(users.get(i).getPassword());
//			
//			System.out.println();
//						
//		}
//		System.out.println();
//		
//		Password VPR21 = new Password("aquila");
//		Password APL14 = new Password("eastpack");
//		Password GBU11 = new Password("1234");
//		Password VCP40 = new Password("kiwi");
//		Password ADE32 = new Password("pineapplepen");
//		
//		coll.updatePassword("VPR21", VPR21.getPassHashcode());
//		coll.updatePassword("APL14", APL14.getPassHashcode());
//		coll.updatePassword("GBU11", GBU11.getPassHashcode());
//		coll.updatePassword("VCP40", VCP40.getPassHashcode());
//		coll.updatePassword("ADE32", ADE32.getPassHashcode());
//		
//		users = coll.getUserList(coll.loadAllUsers());
//		for (int i = 0;i<5; i++){
//			System.out.println(users.get(i));
//			String passInDB = new String(users.get(i).getPassword());
//			System.out.println(passInDB);
////			password = new Password(users.get(i).getPassword());
//			
//			System.out.println();
//						
//		}
//		ArrayList<Admin> admins = coll.getAdminList(coll.loadAllAdmins());
//		for (Admin ad : admins){
//			System.out.println(ad);
//		}
//		System.out.println();
//		
//		ArrayList<Customer> custs = coll.getCustomerList(coll.loadAllCustomers());
//		for (Customer cust: custs){
//			System.out.println(cust);
//		}
//		System.out.println();
//		
//		Customer cus = coll.searchCustomerByTax("ADE32");
//		if (cus!=null){
//			System.out.println(cus);	
//			}else{
//				System.out.println("Sorry,could not find customer");
//			}
//		System.out.println();
//		
//		cus = coll.searchCustomerByEmail("budgeorgi@yahoo.it");
//		if (cus!=null){
//			System.out.println(cus);	
//			}else{
//				System.out.println("Sorry,could not find customer");
//			}
//		
//	}

	/**
	 * Constructor
	 */
	public UserCollection(){
		//instantiates the database manager to be used in this class
		dbm = new DBManager();
	}
	
	
	/**
	 * Method to save the whole list of updated users in database.
	 * To be used in the tables of admin or customers when saving data.
	 * Takes :
	 * @param list
	 * An arrayList of either Admin or Customer, to be processed:
	 * in the method each of the element in the list creates an active user and
	 * is saved in db; the methods updateUserData(ActiveUser au) takes care of updating
	 * all the tables necessary. 
	 * @return
	 */
	public <U extends CUser> boolean updateAllUsers(ArrayList<U> list ){
		boolean done = false;
		ActiveUser au;
		for(int i = 0; i<list.size(); i++){
			au = new ActiveUser(list.get(i));
			done = updateUserData(au);
			if (done == false) {
				System.out.println("Update of users cannot be completed."
						+ "\nElements updated: " + i+1);
			}
		}
		
		return done;
	}
	
	/**
	 * Use this to update the data in the tables cuser, admin/customer alltogether.
	 * @param tax_no
	 * @param au
	 * @return
	 */
	public boolean updateUserData(ActiveUser au){
		
		boolean done = true;
		
		Admin adm;
		Customer cust;
		
		updateInCUser(au.getUser());
		
		done = false;
		
		if (au.isAdmin()){
			adm = au.getActiveAdmin();
			done = updateAdmin(adm);
		}
		else if(!au.isAdmin()){
			cust = au.getActiveCust();
			done = updateCust(cust);
		}
		
		return done;
	}

	private boolean updateCust(Customer cust) {
		boolean done;
		String tax = cust.getTax_no();
		
		//cannot change registration date because it is generated automatically
		String updateC = "UPDATE customer SET id_mem = " + 
				((cust.getId_mem() == 0)?null:cust.getId_mem())+
				" WHERE tax_no = '" + tax + "';";
		done = dbm.updateSql(updateC);
		dbm.closeSets();
		
		debugInfoUpdateUser(done,"customer");
		
		return done;
	}

	private boolean updateAdmin(Admin admin){
		
		boolean done;
		String tax = admin.getTax_no();
		
		String updateA = "UPDATE admin SET permission = " + admin.getPermission() + 
				" WHERE tax_no = '" + tax + "';";
		
		done = dbm.updateSql(updateA);
		dbm.closeSets();
		
		debugInfoUpdateUser(done,"admin");
		
		return done;
	}
	
	private boolean updateInCUser(CUser user){
		boolean done;
		String tax = user.getTax_no();
		
		//type of user and password must not be updated
		String update = "UPDATE cuser SET name = '" +user.getName()+"', email = '"
				+ user.getEmail() + "',phone_no = '" + user.getPhone_no() +"', gender = '" +
				user.getGender() + "', date_birth = '" + user.getDate_birth().toString() +"' "
						+ "WHERE tax_no = '" + tax + "';";
		
		done = dbm.updateSql(update);
		dbm.closeSets();
		
		debugInfoUpdateUser(done,"user");
		
		return done; 
	}
	
	private void debugInfoUpdateUser(boolean done, String where){
		if (done){
			System.out.println("Update of " + where +" completed.");
			} else {
				System.out.println("Could not update " + where +" properly.");
			}
	}
	
	public boolean updatePassword(String userNO,char[] pass){
		boolean done;
		
		String update = "UPDATE cuser SET password = '" + new String(pass) +
				"' WHERE tax_no = '" + userNO +"';";
		
		done = dbm.updateSql(update);
		dbm.closeSets();
		
		debugInfoUpdateUser(done,"password in user");
		
		return done;
	}
	
	/**
	 * Adds the user to the database through the instance of active user;
	 * This allows the automatic check and insertion also of the values in the 
	 * admin of customer tables.
	 * 	 * @param au
	 * @return
	 */
	public boolean addUser(ActiveUser au){
		
		boolean done;
		Admin adm;
		Customer cust;
		
		insertCUser(au.getUser());
		
		done = false;
		
		if (au.isAdmin()){
			adm = au.getActiveAdmin();
			done = insertAdmin(adm);
		}
		else if(!au.isAdmin()){
			cust = au.getActiveCust();
			done = insertCust(cust);
		}
		return done;
	}
	
	private boolean insertCUser(CUser user){
		
		boolean done;
		String insertU = "INSERT into CUser (tax_no , name, email, phone_no, password,"
				+ " gender, date_birth, type) VALUES ";
		char[] password = user.getPassword();
		
		insertU += "('"+ user.getTax_no() + "','" + user.getName() + "','" + user.getEmail() + "','"+
				user.getPhone_no()+ "','" + new String(password) + "','" + user.getGender() +
				"','" + user.getDate_birth().toString() + "','" + user.getType() + "');";
		
		done = dbm.updateSql(insertU);
		dbm.closeSets();
		
		if (done){
		System.out.println("Insertion of user completed.");
		} else {
			System.out.println("Could not insert user properly.");
		}
		
		return done;
		
	}
	
	private boolean insertAdmin(Admin admin){
		boolean done;
		String insertA = "INSERT into Admin(tax_no, permission) VALUES";
		insertA += "('" + admin.getTax_no() + "'," + admin.getPermission()+");";
		done = dbm.updateSql(insertA);
		dbm.closeSets();
		
		if (done){
		System.out.println("Insertion of admin completed.");
		} else {
			System.out.println("Could not insert admin properly.");
		}
		
		return done;
	}
	
	private boolean insertCust(Customer cust){
		boolean done;
		String insertC = "INSERT into Customer(tax_no, reg_date, id_mem) VALUES";
		insertC += "('" + cust.getTax_no() + "','" + cust.getReg_date().toString() + "'," + 
							((cust.getId_mem()==0)? null:cust.getId_mem()) + ");";
		done = dbm.updateSql(insertC);
		dbm.closeSets();
		
		if (done){
		System.out.println("Insertion of customer completed.");
		} else {
			System.out.println("Could not insert customer properly.");
		}
		
		return done;
	}

	public boolean deleteUser(ActiveUser au){
		boolean done;
		
		if (au.isAdmin()){
			done = deleteAdmin(au.getActiveAdmin());
		} else { 
			done = deleteCustomer(au.getActiveCust());
		}
		done = deleteCUser(au.getUser());
		
		return done;
	}
	
	private boolean deleteCUser(CUser user){
		boolean done; 
		String deleteU = "DELETE from cuser WHERE tax_no = '" + user.getTax_no() + "';";
		done = dbm.updateSql(deleteU);
		dbm.closeSets();
		
		if (done){
			System.out.println("Deletion of USER completed.");
			} else {
				System.out.println("Could not delete user properly.");
			}
		
		return done;
	}
	
	private boolean deleteAdmin(Admin ad){
		boolean done;
		String deleteA = "DELETE from admin WHERE tax_no = '" + ad.getTax_no() + "';";
		done = dbm.updateSql(deleteA);
		dbm.closeSets();

		if (done){
		System.out.println("Deletion of admin completed.");
		} else {
			System.out.println("Could not delete admin properly.");
		}
		return done;
	}
	
	private boolean deleteMembershipForCust(int idMem){
		boolean done = true;
		
		String deleteM = "DELETE from membership where id = " 
							+idMem + ";";
		done = dbm.updateSql(deleteM);
		dbm.closeSets();
		
		
		if (done){
			System.out.println("Deletion of membership completed.");
			} else {
				System.out.println("Could not delete membership properly.");
			}
			return done;
	}
	
	private boolean deleteCustomer(Customer cust){
		boolean done;
		int idMem = cust.getId_mem();
		String deleteC = "DELETE from customer WHERE tax_no = '" + cust.getTax_no() + "';";
		done = dbm.updateSql(deleteC);
		dbm.closeSets();
		if (idMem != 0){
			done = deleteMembershipForCust(idMem);
		}
		if (done){
		System.out.println("Deletion of customer completed.");
		} else {
			System.out.println("Could not delete customer properly.");
		}
		return done;
	}


	/**
	 * Always check not null
	 * @param email
	 * @return
	 */
	public Customer searchCustomerByEmail(String email){
		Customer cust = null;
		ArrayList<Customer> foundCust = null;
		String query = "SELECT * FROM cuser AS u,customer AS c WHERE UPPER(u.email) LIKE '" + email.toUpperCase()
				+"' AND u.tax_no = c.tax_no;";
		dbm.query(query);
		ResultSet rs = dbm.getRs();
		
		if (rs!=null){
		    foundCust = getCustomerList(rs);
		}
		
		dbm.closeSets();
		
		if (foundCust.size()>0){
			cust = foundCust.get(0);
			}
		return cust;
		
	}
	
	
	/**
	 * Always check not null
	 * @param tax
	 * @return
	 */
	public Customer searchCustomerByTax(String tax){
		
		Customer cust = null;
		ArrayList<Customer> foundCust = null;
		String query = "SELECT * FROM cuser AS u,customer AS c WHERE UPPER(u.tax_no) = '" + tax.toUpperCase() 
				+"' AND u.tax_no = c.tax_no;";
		dbm.query(query);
		ResultSet rs = dbm.getRs();
		
		if (rs!=null){
			
			foundCust = getCustomerList(rs);
			
//			try {
//				while (rs.next()){
//					cust = new Customer();
//					cust.setTax_no(rs.getString("tax_no"));
//					cust.setName(rs.getString("name"));
//					cust.setEmail(rs.getString("email"));
//					cust.setPhone_no(rs.getString("phone_no"));
//					String pass = rs.getString("password");
//
//					char[] password = new char[64];
//					for (int i = 0; i < pass.length(); i++) {
//						password[i] = pass.charAt(i);
//					}
//					cust.setPassword(password);
//					char gen = rs.getString("gender").charAt(0);
//					cust.setGender(gen);
//					char type = rs.getString("type").charAt(0);
//					cust.setType(type);
//					LocalDate dateB = LocalDate.parse(rs.getObject("date_birth").toString());
//					cust.setDate_birth(dateB);
//					LocalDate dateR = LocalDate.parse(rs.getObject("reg_date").toString());
//					cust.setReg_date(dateR);
//					Integer idMem = rs.getInt("id_mem");
//					if (idMem != null) {
//						cust.setId_mem(idMem);
//					} else
//						cust.setId_mem(0);
//					
//					
//				}
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		if (foundCust.size()>0){
		cust = foundCust.get(0);
		}
		dbm.closeSets();
		return cust;
		
		
	}
	

	public Admin searchAdminByTax(String tax){
		
		Admin admin = null;
		ArrayList<Admin> adminFound = null;
		String query = "SELECT * from cuser AS u, admin AS a WHERE UPPER(u.tax_no) = '" +
				tax.toUpperCase() + "' AND u.tax_no = a.tax_no";
		dbm.query(query);
		ResultSet rs = dbm.getRs();
		if (rs!=null){
			
			adminFound = getAdminList(rs);
		
		}
		if (adminFound.size()>0){
			admin = adminFound.get(0);
			}
		dbm.closeSets();
		return admin;
	}
	
	
	public Admin searchAdminByEmail(String email){
		Admin admin = null;
		ArrayList<Admin> adminFound = null;
		String query = "SELECT * from cuser AS u, admin AS a WHERE UPPER(u.email) LIKE '" +
				email.toUpperCase() + "' AND u.tax_no = a.tax_no";
		dbm.query(query);
		ResultSet rs = dbm.getRs();
		if (rs != null){
			adminFound = getAdminList(rs);
//		try {
//		while(rs.next()){
//			
//				admin = new Admin();
//				admin.setTax_no(rs.getString("tax_no"));
//				admin.setName(rs.getString("name"));
//				admin.setEmail(rs.getString("email"));
//				admin.setPhone_no(rs.getString("phone_no"));
//				String pass = rs.getString("password");
//				
//				char[] password = new char[64];
//				for (int i=0;i<pass.length();i++){
//					password[i]=pass.charAt(i);
//				}
//				admin.setPassword(password);
//				char gen = rs.getString("gender").charAt(0);
//				admin.setGender(gen);
//				char type = rs.getString("type").charAt(0);
//				admin.setType(type);
//				LocalDate date = LocalDate.parse(rs.getObject("date_birth").toString());
//				admin.setDate_birth(date);
//				admin.setPermission(rs.getInt("Permission"));
//		}
//		} catch (SQLException e) {
//	// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		}
		if (adminFound.size()>0){
		admin = adminFound.get(0);
		}
		dbm.closeSets();
		return admin;
	}
	
	/**
	 * Gets the tuples in Admin natural join Cuser tables of the database
	 * @return
	 */
	public ResultSet loadAllAdmins(){
		
		ResultSet rs;
		dbm.query("Select * from cuser,admin where cuser.tax_no = admin.tax_no"
				+ "  ORDER BY admin.permission, admin.tax_no ;");
		rs = dbm.getRs();
		
		return rs;
	}

	
	/**
	 * Gets the tuples from CUser table
	 * @return
	 */
	public ResultSet loadAllUsers(){
		ResultSet rs;
		dbm.query("Select * from CUser"
				+ " ORDER BY type,tax_no");
		rs = dbm.getRs();
		
		return rs;
	}
	
	/**
	 * Gets all the customers together with their cuser details
	 * @return
	 */
	
	public ResultSet loadAllCustomers(){
		ResultSet rs;
		dbm.query("Select * from cuser,customer where cuser.tax_no = customer.tax_no "
				+ "ORDER BY cuser.tax_no");
		rs = dbm.getRs();
		
		return rs;
	}
	
	/**
	 * Gets the list of users in the result set of a query as arraylist.
	 * @param rs
	 * Always input as parameter the result set of the query which returns all 
	 * attributes of the user table
	 * @return
	 */
	
	public ArrayList<CUser> getUserList(ResultSet rs) {

		userList = new ArrayList<>();
		if (rs != null)
		try {
			while (rs.next()) {
				String tax = rs.getString("tax_no");
				String name = rs.getString("name");
				String email = rs.getString("email");
				String phone = rs.getString("phone_no");
				String pass = rs.getString("password");
				char gen = rs.getString("gender").charAt(0);
				char type = rs.getString("type").charAt(0);
				LocalDate date = LocalDate.parse(rs.getObject("date_birth").toString());

				char[] password = new char[64];
				for (int i = 0; i < pass.length(); i++) {
					password[i] = pass.charAt(i);
				}

				user = new CUser(tax, name, email, phone, password, gen, type, date);
				userList.add(user);
				// System.out.println(user.toString());

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{

		dbm.closeSets(); // I close connection for this query
		}
		
		return userList;

	}
	
	/**
	 * Returns a list of admins as arraylist.
	 * @param rs 
	 * Always input as parameter a ResultSet which contains the joined
	 * tables cuser and admin
	 * @return arrayList of admin
	 */
	public ArrayList<Admin> getAdminList(ResultSet rs){
		adminList = new ArrayList<>();

		try {
			while(rs.next()){
				try {
					admin = new Admin();
					admin.setTax_no(rs.getString("tax_no"));
					admin.setName(rs.getString("name"));
					admin.setEmail(rs.getString("email"));
					admin.setPhone_no(rs.getString("phone_no"));
					String pass = rs.getString("password");
					
					char[] password = new char[64];
					for (int i=0;i<pass.length();i++){
						password[i]=pass.charAt(i);
					}
					admin.setPassword(password);
					char gen = rs.getString("gender").charAt(0);
					admin.setGender(gen);
					char type = rs.getString("type").charAt(0);
					admin.setType(type);
					LocalDate date = LocalDate.parse(rs.getObject("date_birth").toString());
					admin.setDate_birth(date);
					admin.setPermission(rs.getInt("Permission"));
					
//					System.out.println(admin);
					
					adminList.add(admin);
				
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
		dbm.closeSets(); //I close connection for this query
		}
		return adminList;
	}
	
	
	
	/**
	 * Returns a list of customers as arraylist.
	 * @param rs 
	 * Always input as parameter a ResultSet which contains the joined
	 * tables cuser and customer
	 * @return arrayList of customer
	 */
	public ArrayList<Customer> getCustomerList(ResultSet rs) {
		customerList = new ArrayList<>();
		
		if (rs != null)
		try {
			while (rs.next()) {
				try {
					customer = new Customer();
					customer.setTax_no(rs.getString("tax_no"));
					customer.setName(rs.getString("name"));
					customer.setEmail(rs.getString("email"));
					customer.setPhone_no(rs.getString("phone_no"));
					String pass = rs.getString("password");

					char[] password = new char[64];
					for (int i = 0; i < pass.length(); i++) {
						password[i] = pass.charAt(i);
					}
					customer.setPassword(password);
					char gen = rs.getString("gender").charAt(0);
					customer.setGender(gen);
					char type = rs.getString("type").charAt(0);
					customer.setType(type);
					LocalDate dateB = LocalDate.parse(rs.getObject("date_birth").toString());
					customer.setDate_birth(dateB);
					LocalDate dateR = LocalDate.parse(rs.getObject("reg_date").toString());
					customer.setReg_date(dateR);
					Integer idMem = rs.getInt("id_mem");
					if (idMem != null) {
						customer.setId_mem(idMem);
					} else
						customer.setId_mem(0);

//					System.out.println(customer);

					customerList.add(customer);

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		dbm.closeSets();
		}
		return customerList;
		
	}

}
