package Utilities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

import DataStructure.Proj_Reserv_Movie;
import DataStructure.Reservation;

public class ReservationCollection {
	
	DBManager dbm;
	ResultSet rs;
	ArrayList<Proj_Reserv_Movie> reservationList;
	Proj_Reserv_Movie PRMOb;
	
	
	public ResultSet reservationQueryUserId(String userId) {
		dbm = new DBManager();
		
		dbm.query("select * from reservation r,projection p, movie m where r.cust_taxno='"+userId+"'and r.proj_id=p.proj_id and p.movie_id=m.id");
		rs = dbm.getRs();
		  
		return rs;
	}
	
	
	
	
	public ResultSet reservationQueryProjId(String projId) {
         dbm = new DBManager();
		
		dbm.query("select p.proj_id,m.title,u.email,u.tax_no,p.day,p.start_time,p.room_no \r\n" + 
				"from projection as p,movie as m,reservation as r,cuser as u\r\n" + 
				"where p.proj_id='"+projId+"' and p.movie_id = m.id and r.proj_id = p.proj_id and r.cust_taxno = u.tax_no");
		rs = dbm.getRs();
		  
		return rs;
		
	}
	
	public ResultSet reservationQueryEmail(String email) {
        dbm = new DBManager();
		
		dbm.query("select p.proj_id, m.title,u.tax_no,p.day,p.start_time,p.room_no from projection as p,movie as m, \r\n" + 
				"reservation as r,cuser as u where p.movie_id = m.id and r.proj_id = p.proj_id and \r\n "+
				"r.cust_taxno =u.tax_no and u.email='"+email+"'");
		rs = dbm.getRs();
		  
		return rs;
		
	}
	
	
	
	public ArrayList<Proj_Reserv_Movie> reservationListProjId(ResultSet rs) {
		reservationList= new ArrayList<Proj_Reserv_Movie>();
		
		try {
			while (rs.next() && rs!=null) {
				
	    		PRMOb= new Proj_Reserv_Movie();
	    		
	    		PRMOb.setProj_id(rs.getInt(1));
	    		PRMOb.setTitle(rs.getString(2));
	    		PRMOb.setEmail(rs.getString(3));
	    		PRMOb.setCust_taxno(rs.getString(4));
	    		PRMOb.setProj_day(rs.getDate(5));
	    		PRMOb.setProj_startTime(rs.getTime(6));
	    		PRMOb.setRoomId(rs.getInt(7));
	    		System.out.println("riempimento PRMb");
	    		reservationList.add(PRMOb);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		dbm.closeSets();
		}
		return reservationList;	
	}
	
	
	public ArrayList<Proj_Reserv_Movie> reservationListUserId(ResultSet rs) {
		reservationList= new ArrayList<Proj_Reserv_Movie>();
		
		try {
			while (rs.next() && rs!=null) {
				
	    		PRMOb= new Proj_Reserv_Movie();
				
				PRMOb.setProj_id(rs.getInt(1));
				PRMOb.setCust_taxno(rs.getString(2));
				PRMOb.setSeat(rs.getString(3));
				PRMOb.setReser_date_time(rs.getTimestamp(4));
				PRMOb.setMovieId(rs.getInt(6));
				PRMOb.setRoomId(rs.getInt(7));
				PRMOb.setProj_day(rs.getDate(8));
				PRMOb.setProj_startTime(rs.getTime(9));
				PRMOb.setPrice(rs.getDouble(10));
				PRMOb.setProj_createdBy(rs.getString(11));
				PRMOb.setProj_creationDateTime(rs.getTimestamp(12));
				PRMOb.setThreeD_Option(rs.getBoolean(13));
		        PRMOb.setTitle(rs.getString(15));
				PRMOb.setDateOfProduction(rs.getDate(16));
				PRMOb.setDuration(rs.getInt(17));
				PRMOb.setCountry(rs.getString(18));
				
		        reservationList.add(PRMOb);
		       
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
		dbm.closeSets();
		}
		return reservationList;
		
	}
	
	
	
	public ArrayList<Proj_Reserv_Movie> reservationListEmail(ResultSet rs) {
		reservationList= new ArrayList<Proj_Reserv_Movie>();
		
		try {
			while (rs.next() && rs!=null) {
				
	    		PRMOb= new Proj_Reserv_Movie();
	    		
	    		PRMOb.setProj_id(rs.getInt(1));
	    		PRMOb.setTitle(rs.getString(2));
	    		PRMOb.setCust_taxno(rs.getString(3));
	    		PRMOb.setProj_day(rs.getDate(4));
	    		PRMOb.setProj_startTime(rs.getTime(5));
	    		PRMOb.setRoomId(rs.getInt(6));
	    		
	    		reservationList.add(PRMOb);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
		dbm.closeSets();
		}
		return reservationList;	
	}

}
