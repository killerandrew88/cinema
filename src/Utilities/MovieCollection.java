package Utilities;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JOptionPane;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

import DataStructure.Movie;
import DataStructure.Proj_Reserv_Movie;

public class MovieCollection {

	DBManager dbm;
	ResultSet rs;
	ArrayList<Movie> movieList;
	ArrayList movieGenre, movieDirector;
	Movie movie;
	Iterator iterator;

	public MovieCollection() {
		dbm= new DBManager();
	}
	
	public ResultSet movieQueryTitle(String title) {
		title.toUpperCase();

		dbm.query("select m.id,m.title,m.dateofprod, m.duration,m.country from movie m where UPPER(m.title)='" + title
				+ "'"); // LIKE
		rs = dbm.getRs();

		return rs;
	}

	public ResultSet movieQueryDateOfProd(Date date) {
		dbm.query("select m.id,m.title,m.dateofprod, m.duration,m.country from movie m where m.dateofprod='" + date
				+ "'"); // LIKE
		rs = dbm.getRs();

		return rs;
	}

	public ResultSet allMovieQuery() {
		dbm.query("select m.id,m.title,m.dateofprod, m.duration,m.country from movie m");
		rs = dbm.getRs();
		return rs;
	}

	public ArrayList<Movie> movieList(ResultSet rs) {
		movieList = new ArrayList<Movie>();

		try {

			while (rs.next() && rs != null) {

				movie = new Movie();

				movie.setId(rs.getInt("Id"));
				movie.setTitle(rs.getString("title"));
				movie.setDateOfProduction(rs.getDate("dateofprod"));
				movie.setDuration(rs.getInt("duration"));
				movie.setCountry(rs.getString("country"));

				movieList.add(movie);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		dbm.closeSets();
		System.out.println(movieList.size());
		assignGenre();
		assignDirector();

		return movieList;
	}

	
	
	public ResultSet allGenreQuery() {
		
		dbm.query("select m.id, mg.genre from movie m, movie_genre mg where m.id=mg.movie_id ORDER BY m.id ASC");
		rs = dbm.getRs();
		
		return rs;
	}
	
	public void assignGenre() {
		rs= allGenreQuery();

		
		ListMultimap<Integer, String> genre = getMultiMap(rs);

		movieGenre = null;
		iterator = movieList.iterator();
		while (iterator.hasNext()) {

			movie = (Movie) iterator.next();
			movieGenre = new ArrayList();
			for (Integer key : genre.keySet()) {
				if (key == movie.getId()) {

					movieGenre.add(genre.get(key));

				}
				movie.setGenre(movieGenre);
			}

			
		}
		dbm.closeSets();

	}
	
	
	public ResultSet allDirectorQuery() {
		
		dbm.query("select d.name from director d ");
		rs = dbm.getRs();
		
		return rs;
	}
	
	public ResultSet DirectorNameIdQuert() {
		
		dbm.query("select d.id, d.name from director d ");
		rs = dbm.getRs();
		
		return rs;
	}

	public void assignDirector() {

		dbm = new DBManager();
		dbm.query(
				"select m.id, d.name from movie m, movie_director md, director d where m.id=md.movie_id  and md.dir_id=d.id ORDER BY m.id ASC");
		rs = dbm.getRs();
		ListMultimap<Integer, String> dir = getMultiMap(rs);
		movieDirector = null;

		iterator = movieList.iterator();
		while (iterator.hasNext()) {
			movie = (Movie) iterator.next();

			movieDirector = new ArrayList();

			for (Integer key : dir.keySet()) {
				if (key == movie.getId()) {

				movieDirector.add(dir.get(key));
				}
			}
			movie.setDirector(movieDirector);
		}
		dbm.closeSets();
	}
	
	public boolean insertNewDirector(String name, Date dateOfBirth) {
		boolean done;
		
		done=dbm.updateSql("insert into director values(default,'"+name+"','"+dateOfBirth+"');");
		dbm.closeSets();
		
		return done;
	}
	
	public boolean newMovie( String title,Date dateofprod,String duration, String country) {
		String query="";
		boolean done;
		
		System.out.println(query);
		query += "insert into movie values (default,'"+title+"','"+dateofprod+"','"+duration+"','"+country+"');" ;
		done=dbm.updateSql(query);
		dbm.closeSets();
		return done;
	}   
	
	public boolean newMovie_genre_director(int movie_id, ArrayList <Integer> dirId,ArrayList<String>genre) {
		String query="";
		boolean done;
		
		for(int i=0; i<genre.size(); i++) //list genre
			query+="insert into movie_genre values ("+movie_id+",'"+genre.get(i)+"');";
			
		for(int i=0;i<dirId.size();i++)// list director
			query+="insert into movie_director values("+movie_id+","+dirId.get(i)+ ");";
		
		done= dbm.updateSql(query);
		dbm.closeSets();
		
		return done;
	}
	
	
	
    public int movieId(String title)  {
    	int id = 0;
    	
    	dbm.query("select m.id from movie m where UPPER(m.title)='"+title.toUpperCase()+"';");
		rs = dbm.getRs();
		
		
		try {
			while(rs.next()) {
			id= rs.getInt("id");
			}
	   } catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return id;
    }
	
    
    
    public void deleteMovie(int movie_id) {
		String query = "DELETE FROM reservation r where r.proj_id IN (select p.proj_id from projection p where p.movie_id=" + movie_id + ");"
				+ "DELETE FROM movie_genre mg where mg.movie_id="+ movie_id+";"
				+"DELETE FROM movie_director md where md.movie_id="+ movie_id+";"
				+" DELETE FROM movie as m WHERE m.id = "+movie_id+";";
		dbm.updateSql(query);
		dbm.closeSets();
	}

    
	public boolean checkDateFormat(String value) {
		java.util.Date date = null;
		Boolean dateFormat = true;
		if(value.isEmpty()) {
//			
			return dateFormat=false;
		}
		if(value.length()<10) {
			return dateFormat=false;
		}
		
		
		else {
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			date = sdf.parse(value);
			if (!value.equals(sdf.format(date))) {
				return dateFormat = false;
			}
			if((date.before(Date.valueOf("1900-01-01")) || date.after(Date.valueOf("2017-01-01")))==true ) {
				return dateFormat=false;
			}
			
		} catch (ParseException ex) {
			
		   }
		}
		if (date == null) {
			return dateFormat = false;
			
		}
		return dateFormat;
	}
	

	public ListMultimap getMultiMap(ResultSet rs) {
		ListMultimap<Integer, String> multimap = ArrayListMultimap.create();

		try {
			while (rs.next()) {
				multimap.put(rs.getInt(1), rs.getString(2));

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return multimap;

	}

}
