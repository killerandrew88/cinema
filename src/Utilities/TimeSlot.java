package Utilities;

import java.time.LocalTime;
import static java.time.temporal.ChronoUnit.MINUTES;

public class TimeSlot {
	private LocalTime starTime;
	private LocalTime endTime;
	
	public TimeSlot(LocalTime startTime, LocalTime endTime) {
		this.starTime = startTime;
		this.endTime = endTime;
	}
	
	public long getDuration() {
		return MINUTES.between(this.starTime	, this.endTime);
	}

	public LocalTime getStarTime() {
		return starTime;
	}

	public void setStarTime(LocalTime starTime) {
		this.starTime = starTime;
	}

	public LocalTime getEndTime() {
		return endTime;
	}

	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}
	

}
